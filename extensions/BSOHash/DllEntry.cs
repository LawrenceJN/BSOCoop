﻿﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;


namespace BSOHash
{

    public static class DllEntry
    {
#if WIN64
        [DllExport("RVExtension")]
#else
        [DllExport("_RVExtension@12", CallingConvention.StdCall)]
#endif
        public static void RVExtension(StringBuilder output, int outputSize, [MarshalAs(UnmanagedType.LPStr)] string input)
        {
            outputSize--;
            if (input != "version")
                return;

            var executingAssembly = Assembly.GetExecutingAssembly();
            try
            {
                var location = executingAssembly.Location;
                if (location == null)
                    throw new Exception("Assembly location not found");
                output.Append(FileVersionInfo.GetVersionInfo(location).FileVersion);
            }
            catch (Exception e)
            {
                output.Append(e.Message);
            }
        }
        public static string GenerateHash(string input)
        {
            int output = 0;
            for (int i = 0; i != input.Length; i++)
            {
                output += Char.ConvertToUtf32(input, (int)i) % (2 ^ 32);
            }
            return "0x" + output.ToString("x").ToUpper();
        }
    }
}
