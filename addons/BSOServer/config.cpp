#include "macros.hpp"
class CfgPatches {
    class BSOServer {
        units[] = {};
        weapons[] = {};
        requiredVersion = 1.56;
        author = "joko // Jonas";
        authors[] = {"joko // Jonas"};
        authorUrl = "https://twitter.com/joko48Ger";
        version = VERSION;
        versionStr = QUOTE(VERSION);
        versionAr[] = {VERSION_AR};
        requiredAddons[] = {"BSOCoop"};
    };
};

#include "\userconfig\BSOCoop\whitelist.hpp"
