#include "macros.hpp"
/*
    BSO Coop

    Author: joko // Jonas

    Description:
    Register the Respawn Zeus modules.

    Parameter(s):
    NONE

    Returns:
    NONE
*/
["BSO Respawn", "Respawn", {
    params ["", "_unit"];

    if (isNull _unit) then {
        private _properties = ["Respawn", [
            ["Respawn type", ["All", "Selection", "Player"]]
        ]] call Ares_fnc_ShowChooseDialog;

        if (_properties isEqualTo []) exitWith { ["Player respawn aborted"] call Achilles_fnc_showZeusErrorMessage; };

        private _playersToRespawn = [];
        switch (_properties select 0) do {
            case 0: {
                _playersToRespawn = allPlayers - entities "HeadlessClient_F";
            };
            case 1: {
                private _selection = [toLower localize "STR_AMAE_PLAYERS"] call Achilles_fnc_SelectUnits;
                if (isNil "_selection") exitWith {};
                _playersToRespawn = _selection select {isPlayer _x};
            };
            case 2: {
                private _selectionList = ((allPlayers - entities "HeadlessClient_F") apply {[name _x, _x]});
                _selectionList sort true;
                private _properties = ["Respawn player", [
                    ["Player", _selectionList apply {_x select 0}]
                ]] call Ares_fnc_ShowChooseDialog;
                if (_properties isEqualTo []) exitWith {};
                _playersToRespawn = [(_selectionList select (_properties select 0)) select 1];
            };
        };

        if (_playersToRespawn isEqualTo []) exitWith { ["No players selected"] call Achilles_fnc_showZeusErrorMessage; };

        _playersToRespawn = (_playersToRespawn select {!alive _x});

        ["respawnPlayer", _playersToRespawn] call CFUNC(targetEvent);

        ["Respawned %1", count _playersToRespawn] call Ares_fnc_showZeusMessage;
    } else {
        if (!isPlayer _unit) exitWith { [localize "STR_AMAE_NOT_PLACED_ON_UNIT"] call Achilles_fnc_showZeusErrorMessage; };

        if (alive _unit) exitWith { ["Player is still alive"] call Achilles_fnc_showZeusErrorMessage; };

        ["respawnPlayer", _unit] call CFUNC(targetEvent);

        ["Respawned %1", name _unit] call Ares_fnc_showZeusMessage;
    };
}] call Ares_fnc_RegisterCustomModule;

["BSO Respawn", "Wave Respawn Settings", {
    private _properties = ["Wave Respawn Settings", [
        ["Respawn Time (min) [Disabled: -1]", "", "30"]
    ]] call Ares_fnc_ShowChooseDialog;

    if (_properties isEqualTo []) exitWith { ["Aborted"] call Achilles_fnc_showZeusErrorMessage; };
    GVAR(WaveRespawnTime) = parseNumber (_properties select 0);
    if (GVAR(WaveRespawnTime) != -1) then {
        GVAR(WaveRespawnTime) = (GVAR(WaveRespawnTime) * 60) + 30;
    };
    publicVariable QGVAR(WaveRespawnTime);
}] call Ares_fnc_RegisterCustomModule;
