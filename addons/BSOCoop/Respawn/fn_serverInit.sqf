#include "macros.hpp"
/*
    BSO Coop

    Author: joko // Jonas

    Description:
    Initialize the Respawn module on the server.

    Parameter(s):
    NONE

    Returns:
    NONE
*/
GVAR(WaveRespawnTime) = -1;
publicVariable QGVAR(WaveRespawnTime);
[{
    if (GVAR(WaveRespawnTime) == -1) exitWith {};
    if ((time mod GVAR(WaveRespawnTime)) >= (GVAR(WaveRespawnTime) - 35)) then {
        private _targets = allPlayers select {!alive _x};
        if !(_targets isEqualTo []) then {
            ["respawnPlayer", _targets] call CFUNC(targetEvent);
        };
    };
}, 1] call CFUNC(addPerFrameHandler);
