#include "macros.hpp"
/*
    BSO Coop

    Author: joko // Jonas

    Description:
    Initialize the Respawn module on the client.

    Parameter(s):
    NONE

    Returns:
    NONE
*/
if (call EFUNC(Common,AchillesLoaded)) then {
    call FUNC(zeusModule);
};

["Killed", {
    (_this select 0) params ["_unit"];
    if !(isNil "bis_fnc_moduleRemoteControl_unit" && (_unit isEqualTo player) && (isPlayer _unit)) exitWith {};
    setPlayerRespawnTime 10e10;

    // PP FX
    "dynamicBlur" ppEffectEnable true;
    "dynamicBlur" ppEffectAdjust [0.0];
    "dynamicBlur" ppEffectCommit 5;

    [{
        cutText ["", "BLACK OUT", 4, true];
    }, 1] call CFUNC(wait);

    [{
        cutText ["", "BLACK IN",  5, true];
        "dynamicBlur" ppEffectAdjust [0];
        "dynamicBlur" ppEffectCommit 0;
        ["Initialize", [player, [playerSide], false, true, true, false, true, false, true, false]] call BIS_fnc_EGSpectator;
    }, 6] call CFUNC(wait);
}] call CFUNC(addEventhandler);


["Respawn", {
    clearRadio;

    cutText ["", "BLACK OUT", 1, true];

    [{
        ["Terminate"] call BIS_fnc_EGSpectator;
    }, 1] call CFUNC(wait);

    [{
        cutText ["", "BLACK IN", 5, true];

        "dynamicBlur" ppEffectEnable true;
        "dynamicBlur" ppEffectAdjust [10];
        "dynamicBlur" ppEffectCommit 0;
        "dynamicBlur" ppEffectAdjust [0.0];
        "dynamicBlur" ppEffectCommit 4;
    }, 2] call CFUNC(wait);
}] call CFUNC(addEventhandler);

GVAR(respawnRunning) = false;
["respawnPlayer", {
    if (GVAR(respawnRunning)) exitWith {};
    GVAR(respawnRunning) = true;
    setPlayerRespawnTime 5;
    [{
        if (playerRespawnTime < 0 && alive player) exitWith {
            GVAR(respawnRunning) = false;
            (_this select 1) call CFUNC(removePerFrameHandler);
        };
        [
            format [
                "Time until redeployment: %1",
                [playerRespawnTime, "MM:SS"] call BIS_fnc_secondsToString
            ],
            -1, -1, 1, 0
        ] spawn BIS_fnc_dynamicText;
    }, 1] call CFUNC(addPerFrameHandler);
}] call CFUNC(addEventhandler);
