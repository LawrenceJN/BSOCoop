#include "\tc\CLib\addons\CLib\ModuleMacros.hpp"

class CfgCLibModules {
    class BSO {
        path = "\bso\BSOCoop\addons\BSOCoop"; // TODO add Simplifyed Macro for this
        dependency[] = {"CLib"};

        MODULE(ACE_CPR) {
            dependency[] = {"BSO/Common"};
            FNC(clientInit);
        };

        MODULE(ACRE) {
            dependency[] = {"BSO/Common"};
            FNC(clientInit);
        };

        MODULE(Artillery) {
            dependency[] = {"BSO/Common"};
            FNC(clientInit);
            FNC(init);
            FNC(serverInit);
        };

        MODULE(BFT_light) {
            dependency[] = {"BSO/Common"};
            FNC(addGroupToTracker);
            FNC(addUnitLineToTracker);
            FNC(addUnitToTracker);
            FNC(addVehicleToTracker);
            FNC(bftdialog);
            FNC(clientInit);
            FNC(editButton);
            FNC(getDefaultIcon);
            FNC(isValidUnit);
            FNC(onHoverIn);
            FNC(onHoverOut);
            FNC(updatePlayerMarkers);
            FNC(updateGroupMarkers);
        };

        MODULE(Caching) {
            dependency[] = {"BSO/Common"};
            FNC(clientInit);
            FNC(init);
            FNC(Modules);
            FNC(serverInit);
        };

        MODULE(Common) {
            dependency[] = {"CLib"};
            FNC(AchillesLoaded);
            FNC(clientInit);
            FNC(flattenArray);
            FNC(init);
            FNC(isNearBase);
            FNC(portToACEArsenal);
            FNC(serverInit);
            FNC(speedModifier);
            FNC(teamColours);
            FNC(VOIP);
            FNC(whitelist);
        };

        MODULE(EarPlugs) {
            dependency[] = {"BSO/Common"};
            FNC(clientInit);
        };

        MODULE(MedicBuildings) {
            dependency[] = {"BSO/Common"};
            FNC(clientInit);
        };

        MODULE(ScoreBoard) {
            dependency[] = {"BSO/Common"};
            FNC(init);
            FNC(clientInit);
            FNC(killMonitor);
        };

        MODULE(Respawn) {
            dependency[] = {"BSO/Common"};
            FNC(clientInit);
            FNC(serverInit);
            FNC(zeusModule);
        };

        MODULE(TFAR) {
            dependency[] = {"BSO/Common"};
            FNC(clientInit);
            FNC(serverInit);
        };

        MODULE(Zeus) {
            dependency[] = {"BSO/Common"};
            FNC(AchillesModules);
            FNC(clientInit);
            FNC(draw3D);
            FNC(hcInit);
            FNC(init);
            FNC(missionEndRunTime);
            FNC(serverInit);
            FNC(setGlobalAISkill);
        };
    };
};
