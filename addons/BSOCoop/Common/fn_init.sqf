#include "macros.hpp"
/*
    BSO Coop

    Author: joko // Jonas

    Description:
    initilize Common Module

    Parameter(s):
    None

    Returns:
    None
*/
GVAR(enableDynamicGroups) = ([CFGCOMMON(enableDynamicGroups), 1] call CFUNC(getSetting)) == 1;

["addPlayerScores", {
    (_this select 0) params ["_unit", "_data"];
    if !(isServer) exitWith {
        LOG("Event addPlayerScores dont get Executed on server");
        ["addPlayerScores", _this] call CFUNC(serverEvent);
    };
    _unit addPlayerScores _data;
}] call CFUNC(addEventhandler);

["reveal", {
    (_this select 0) params ["_unit", "_target", "_value"];
    if !(local _unit) exitWith {
        LOG("Reveal event dont got executed on the unit local client");
        ["reveal", _unit, [_unit, _target, _value]] call CFUNC(targetEvent);
    };
    _unit reveal [_target, _value];
}] call CFUNC(addEventhandler);

["requestMove", {
    (_this select 0) params ["_unit", "_target"];
    if !(local _unit) exitWith {
        ["doMove", _unit, [_unit, _target]] call CFUNC(targetEvent);
    };
    _unit move _target;
}] call CFUNC(addEventhandler);

["playerChanged", {
    (_this select 0) params ["_new", "_old"];
    _new setVariable ['CUP_Jumping_Condition', true, true];
    _old setVariable ['CUP_Jumping_Condition', true, true];
}] call CFUNC(addEventhandler);

["entityCreated", {
    params ["_obj"];
    if (_obj isKindOf "LaserTarget") then {
        _obj setVariable ["CLib_noClean", true, true];
        ["enableDynamicSimulation", [_obj, false]] call CFUNC(serverEvent);
        GVAR(protectedObjects) pushBackUnique _obj;
    };
}] call CFUNC(addEventhandler);

GVAR(protectedObjects) = [];
[{
    private _delete = false;
    {
        if (isNull _x) then {
            _delete = true;
        };
        if (dynamicSimulationEnabled _x) then {
            ["enableDynamicSimulation", [_x, false]] call CFUNC(serverEvent);
        };
    } count GVAR(protectedObjects);
    if (_delete) then {
        GVAR(protectedObjects) = GVAR(protectedObjects) - [objNull];
    };
}, 30] call CFUNC(addPerFrameHandler);
