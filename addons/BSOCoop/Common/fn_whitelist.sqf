#include "macros.hpp"
/*
    BSO Coop

    Author: joko // Jonas

    Description:
    initilize Common Module

    Parameter(s):
    None

    Returns:
    None
*/
private _playerUID = getPlayerUID player;
private _setting = [format ["BSO/CfgWhitelist/Units/%1", str player], []] call CFUNC(getSetting);
if !(_setting isEqualTo []) then {
    private _kick = true;
    {
        if (_playerUID in ([format ["BSO/CfgWhitelist/PlayerGroups/%1", _x]] call CFUNC(getSetting))) then {
            _kick = false;
        };
    } count _setting;
    if (_kick) then {
        ["Reserved Slot Please Choose a other Slot"] call BIS_fnc_errorMsg;
        ["epicFail", false, 2] call BIS_fnc_endMission;
    };
};
