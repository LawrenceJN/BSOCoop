#include "macros.hpp"
/*
    BSO Coop

    Author: Lawrence

    Description:
    fix for team colours automatically resetting

    Parameter(s):
    None

    Returns:
    None
*/

["assignedTeamChanged", {
    if (isNil QGVAR(lastGroup)) exitWith {
        GVAR(lastGroup) = group CLib_Player;
    };
    if !(GVAR(lastGroup) isEqualTo (group CLib_Player)) exitWith {
        GVAR(lastGroup) = group CLib_Player;
    };
    (_this select 0) params ["_newTeam","_oldTeam"];
    if (_newTeam == "MAIN") then {
        CLib_Player assignTeam _oldTeam;
    };
}, []] call CFUNC(addEventHandler);