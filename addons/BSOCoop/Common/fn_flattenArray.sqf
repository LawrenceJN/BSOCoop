#include "macros.hpp"
/*
    BSO Coop

    Author: joko // Jonas

    Description:
    flattens array of arrays.
    all flattened elements must be strings

    Parameter(s):
    0 - Array of arrays

    Returns:
    array of strings
*/

private _return = [];
{
    if (_x isEqualType "") then {
        if (_x != "") then {
            _return pushBack _x;
        };
    } else {
        _return append (_x call FUNC(flattenArray));
    };
    nil
} count _this;
_return 