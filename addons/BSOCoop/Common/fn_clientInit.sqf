#include "macros.hpp"
/*
    BSO Coop

    Author: joko // Jonas

    Description:
    initilize Common Module

    Parameter(s):
    None

    Returns:
    None
*/
if (GVAR(enableDynamicGroups)) then {
    ["missionStarted", {
        [{
            ["InitializePlayer", [CLib_player, true]] call BIS_fnc_dynamicGroups;
        }, {
            !isNull CLib_player
        }, []] call CFUNC(waitUntil);
    }] call CFUNC(addEventhandler);
};
GVAR(lastStatus) = false;
[{
    if (isNull curatorCamera && GVAR(lastStatus)) then {
        "exitZeus" call CFUNC(localEvent);
        GVAR(lastStatus) = false;
    };
    if (!(isNull curatorCamera) && !GVAR(lastStatus)) then {
        "enterZeus" call CFUNC(localEvent);
        GVAR(lastStatus) = true;
    };
}, 0] call CFUNC(addPerFrameHandler);

["sideChanged", {
    (_this select 0) params ["_currentSide"];

    if (_currentSide == sideEnemy) then {
        private _rating = rating CLib_Player;
        CLib_Player addRating (0 - _rating);
    };
}] call CFUNC(addEventhandler);

call FUNC(speedModifier);
call FUNC(teamColours);

DFUNC(isBobCat) = {
    private _veh = vehicle CLib_Player;
    if !(_veh isKindOf "B_APC_Tracked_01_CRV_F") exitWith {false};
    if !(driver _veh == CLib_Player) exitWith {false};
    true
};

[
    "Lower Plow",
    CLib_Player,
    0,
    {call FUNC(isBobCat) && (vehicle CLib_player) animationSourcePhase "MovePlow" == 0},
    {
        (vehicle CLib_player) animateSource ["MovePlow", 1];
    }, ["showWindow", false, "ignoredCanInteractConditions", ["isNotInVehicle"]]
] call CFUNC(addAction);

[
    "Raise Plow",
    CLib_Player,
    0,
    {call FUNC(isBobCat) && (vehicle CLib_player) animationSourcePhase "MovePlow" == 1},
    {
        (vehicle CLib_player) animateSource ["MovePlow", 0];
    }, ["showWindow", false, "ignoredCanInteractConditions", ["isNotInVehicle"]]
] call CFUNC(addAction);

call FUNC(whitelist);

GVAR(firstSettingsCall) = false;
[
    "CLib_ShowDebug",
    "CHECKBOX",
    ["Enable Framechart", "Enable Framechart"],
    "CLib",
    false,
    nil,
    {
        params ["_value"];
        if !(GVAR(firstSettingsCall)) exitWith {GVAR(firstSettingsCall) = true;};
        private _call = false;
        if (_value) then {
            if (CLib_PerformanceInfo_pfhID == -1) then {
                _call = true;
            };
        } else {
            if (CLib_PerformanceInfo_pfhID != -1) then {
                _call = true;
            };
        };

        if (_call) then {
            call CLib_PerformanceInfo_fnc_toggleFrameInfo;
        };
    }
] call CBA_Settings_fnc_init;

call FUNC(portToACEArsenal);

GVAR(MobileRespawnPoints) = [];
[{
    {
        _x params ["_obj", "_mobileRespawnData", "_status"];
        if !(alive _obj) then {
            _mobileRespawnData call BIS_fnc_removeRespawnPosition;
            GVAR(MobileRespawnPoints) deleteAt _forEachIndex;
        };
        /*
        private _newStatus = (isNull attachedTo _obj);
        if !(_newStatus isEqualTo _status) then {
            if (_status) then {
                ["enable", uiNamespace getVariable "BIS_RscRespawnControlsMap_ctrlLocList", _mobileRespawnData select 1, "Respawn Fucker"] call BIS_fnc_showRespawnMenuDisableItem;
            } else {
                ["disable", uiNamespace getVariable "BIS_RscRespawnControlsMap_ctrlLocList", _mobileRespawnData select 1, "No Respawn Fucker"] call BIS_fnc_showRespawnMenuDisableItem;
            };
            _x set [2, _newStatus];
        };
        */
        nil
    } forEach GVAR(MobileRespawnPoints);

}, 1] call CFUNC(addPerFrameHandler);

if (isServer) then {
    ["entityCreated", {
        (_this select 0) params ["_obj"];
        if !(_obj getVariable [QGVAR(isMobileRespawn), false]) exitWith {};
        private _mobileRespawnData = [missionNamespace, _obj, "Mobile Respawn"] call BIS_fnc_addRespawnPosition;
        GVAR(MobileRespawnPoints) pushBackUnique [_obj, _mobileRespawnData, true];
        publicVariable QGVAR(MobileRespawnPoints);
    }] call CFUNC(addEventhandler);
};
