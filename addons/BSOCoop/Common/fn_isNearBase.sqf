#include "macros.hpp"
/*
    BSO Coop

    Author: joko // Jonas

    Description:
    checks if a Player is Near/in Base

    Parameter(s):
    None

    Returns:
    bool if player is Near/in Base
*/

private _markers = ["respawn", "respawn_west", "respawn_east", "respawn_guer", "respawn_civilian", "base"];
private _ret = {
    (CLib_Player distance2D (getMarkerPos _x)) <= 1000;
} count _markers;

_ret != 0;
