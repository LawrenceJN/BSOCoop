#include "macros.hpp"
/*
    BSO Coop

    Author: Lawrence

    Description:
    None

    Parameter(s):
    None

    Returns:
    None
*/
if (GVAR(enableDynamicGroups)) then {
    ["missionStarted", {
        ["Initialize", [true, 99, false]] call BIS_fnc_dynamicGroups;
    }] call CFUNC(addEventhandler);
};
