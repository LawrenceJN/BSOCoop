#include "macros.hpp"
/*
    BSO Coop

    Author: joko // Jonas

    Description:
    

    Parameter(s):
    None

    Returns:
    bool if player is Near/in Base
*/
if (isClass (configfile >> "CfgPatches" >> "acre_api")) exitWith {1};
if (isClass (configFile >> "CfgPatches" >> "task_force_radio")) exitWith {2};
0
