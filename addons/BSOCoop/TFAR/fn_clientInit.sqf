#include "macros.hpp"
/*
    BSO Coop
    Author: joko // Jonas
    Description:
    initilize TFAR Settings System
    Parameter(s):
    None
    Returns:
    None
*/
//call compile preprocessFileLineNumbers "\task_force_radio\functions\common.sqf";
DFUNC(tfarSettings) = {

    params [["_onlyLR", false, [false]]];
    //frequencies
    //blufor
    private _strPlayer = str CLib_Player;

    private _SR1 = "30";
    private _SR2 = "31";
    private _SR3 = "32";
    private _SR4 = "33";
    private _SR5 = "34";
    private _SR6 = "35";
    private _SR7 = "36";
    private _SR8 = "37";

    private _SRA = -1;


    private _LR1 = "50";
    private _LR2 = "51";
    private _LR3 = "52";
    private _LR4 = "60";
    private _LR5 = "61";
    private _LR6 = "62";
    private _LR7 = "70";
    private _LR8 = "71";
    private _LR9 = "72";

    private _LRA = -1;


    //Setting Alpha Leads SW Radios
    if (_strPlayer in ["ASL1", "ASL2"]) then {
      _SR1 = "31";
      _SR2 = "30";
      _SR3 = "66";
      _LR1 = "31";
      _LR2 = "30";
      _SRA = 1;

    };

    //Setting Alpha FTL1 SW radio
    if (_strPlayer in ["A11"]) then {
        _SR1 = "31";
        _SR2 = "31.1";
        _SR3 = "66";
    };


    //Setting Alpha FT1 SW radios
    if (_strPlayer in ["A12", "A13", "A14"]) then {
        _SR1 = "31";
        _SR2 = "31.1";
        _SR3 = "66";

    };

    //Setting Alpha FTL2 SW radio
    if (_strPlayer in ["A21"]) then {
        _SR1 = "31";
        _SR2 = "31.2";
        _SR3 = "66";
    };

    //Setting Alpha FT2 SW radios
    if (_strPlayer in ["A22", "A23", "A24"]) then {
        _SR1 = "31";
        _SR2 = "31.2";
        _SR3 = "66";
    };

    //Setting Bravo Leads SW Radios
    if (_strPlayer in ["BSL1", "BSL2"]) then {
      _SR1 = "32";
      _SR2 = "30";
      _SR3 = "66";
      _SRA = 1;
    };

    //Setting Bravo FTL1 SW radio
    if (_strPlayer in ["B11"]) then {
        _SR1 = "32";
        _SR2 = "32.1";
        _SR3 = "66";
    };


    //Setting Bravo FT1 SW radios
    if (_strPlayer in ["B12", "B13", "B14"]) then {
        _SR1 = "32";
        _SR2 = "32.1";
        _SR3 = "66";
    };

    //Setting Bravo FTL2 SW radio
    if (_strPlayer in ["B21"]) then {
        _SR1 = "32";
        _SR2 = "32.2";
        _SR3 = "66";
    };

    //Setting Bravo FT2 SW radios
    if (_strPlayer in ["B22", "B23", "B24"]) then {
        _SR1 = "32";
        _SR2 = "32.2";
        _SR3 = "66";
    };

    //Setting Charlie Leads SW Radios
    if (_strPlayer in ["CSL1", "CSL2"]) then {
        _SR1 = "33";
        _SR2 = "30";
        _SR3 = "66";
        _LR2 = "33";
        _SRA = 1;
        _LRA = 1;
    };

    //Setting Charlie FTL1 SW radio
    if (_strPlayer in ["C11"]) then {
        _SR1 = "33";
        _SR2 = "33.1";
        _SR3 = "66";
    };


    //Setting Charlie FT1 SW radios
    if (_strPlayer in ["C12", "C13", "C14"]) then {
        _SR1 = "33";
        _SR2 = "33.1";
        _SR3 = "66";
    };

    //Setting Charlie FTL2 SW radio
    if (_strPlayer in ["C21"]) then {
        _SR1 = "33";
        _SR2 = "33.2";
        _SR3 = "66";
    };

    //Setting Charlie FT2 SW radios
    if (_strPlayer in ["C22", "C23", "C24"]) then {
        _SR1 = "33";
        _SR2 = "33.2";
        _SR3 = "66";
    };

    //Setting Delta Leads SW Radios
    if (_strPlayer in ["DSL1", "DSL2"]) then {
        _SR1 = "34";
        _SR2 = "30";
        _SR3 = "66";
        _LR2 = "34";
        _SRA = 1;
        _LRA = 1;
    };

    //Setting Delta FTL1 SW radio
    if (_strPlayer in ["D11"]) then {
        _SR1 = "34";
        _SR2 = "34.1";
        _SR3 = "66";
    };


    //Setting Delta FT1 SW radios
    if (_strPlayer in ["D12", "D13", "D14"]) then {
        _SR1 = "34";
        _SR2 = "34.1";
        _SR3 = "66";
    };

    //Setting Delta FTL2 SW radio
    if (_strPlayer in ["D21"]) then {
        _SR1 = "34";
        _SR2 = "34.2";
        _SR3 = "66";
    };

    //Setting Delta FT2 SW radios
    if (_strPlayer in ["D22", "D23", "D24"]) then {
        _SR1 = "34";
        _SR2 = "34.2";
        _SR3 = "66";
    };

    //Setting AT Squad Lead Radios
    if (_strPlayer in ["AT1"]) then {
      _SR1 = "35";
      _SR2 = "30";
      _SR3 = "66";
      _SRA = 1;
    };
    //Setting AT Squad Radios
    if (_strPlayer in ["AT2", "AT3", "AT4"]) then {
      _SR1 = "35";
      _SR3 = "66";
    };


    //Setting QRF Squad Radios
    if (_strPlayer in ["QRF2", "QRF3", "QRF4"]) then {
      _SR1 = "36";
      _SR3 = "30";
      _LRA = 0;
    };

    //Setting QRF Squad Lead Radio
    if (_strPlayer in ["QRF1"]) then {
      _SR1 = "36";
      _SR2 = "30";
      _SR3 = "66";
      _SRA = 1;
    };


    //Setting Sappers Squad Radios
    if (_strPlayer in ["CE2", "CE3", "CE4"]) then {
      _SR1 = "37";
      _SR3 = "66";
      _LRA = 0;
    };

    //Setting Sappers Squad Lead Radio
    if (_strPlayer in ["CE1"]) then {
      _SR1 = "37";
      _SR2 = "30";
      _SR3 = "66";
      _SRA = 1;
    };

    //Setting CLS Squad Radios
    if (_strPlayer in ["CLS2", "CLS3", "CLS4"]) then {
      _SR1 = "38";
      _SR3 = "66";
      _LRA = 0;
    };

    //Setting CLS Squad Lead Radio
    if (_strPlayer in ["CLS1"]) then {
      _SR1 = "38";
      _SR2 = "30";
      _SR3 = "66";
      _SRA = 1;
    };

    //Setting Mortar Squad Radios
    if (_strPlayer in ["MORT2", "MORT3", "MORT4"]) then {
      _SR1 = "39";
      _SR3 = "66";
      _LRA = 0;
    };

    //Setting Mortar Squad Lead Radio
    if (_strPlayer in ["MORT1"]) then {
      _SR1 = "39";
      _SR2 = "30";
      _SR3 = "66";
      _SRA = 1;
    };

    //Setting Crew1 Squad Radios
    if (_strPlayer in ["CR12", "CR13", "CR14"]) then {
      _SR1 = "70.1";
      _SR3 = "66";
      _LRA = 0;
    };

    //Setting Crew1 Squad Lead Radio
    if (_strPlayer in ["CR11"]) then {
      _SR1 = "70.1";
      _SR2 = "70";
      _SR3 = "66";
      _SRA = 1;
    };

    //Setting Crew2 Squad Radios
    if (_strPlayer in ["CR22", "CR23", "CR24"]) then {
      _SR1 = "70.2";
      _SR3 = "66";
      _LRA = 0;
    };

    //Setting Crew2 Squad Lead Radio
    if (_strPlayer in ["CR21"]) then {
      _SR1 = "70.2";
      _SR2 = "70";
      _SR3 = "66";
      _SRA = 1;
    };

    //Setting Crew3 Squad Radios
    if (_strPlayer in ["CR32", "CR33", "CR34"]) then {
      _SR1 = "70.3";
      _SR3 = "66";
      _LRA = 0;
    };

    //Setting Crew3 Squad Lead Radio
    if (_strPlayer in ["CR31"]) then {
      _SR1 = "70.3";
      _SR2 = "70";
      _SR3 = "66";
      _SRA = 1;
    };

    //Setting Plt Cmd SW radios
    if (_strPlayer in ["PLTCMD", "PLTMEDIC"]) then {
        _SR1 = "30.1";
        _SR2 = "30";
        _SR3 = "31";
        _SR4 = "32";
        _SR5 = "33";
        _SR6 = "34";
        _SR7 = "35";
        _SR8 = "36";
        _LR1 = "30";
        _LR2 = "31";
        _LR3 = "32";
        _LR4 = "33";
        _LR5 = "34";
        _LR6 = "35";
        _LR7 = "36";
        _LR8 = "37";
        _LRA = 0;
        _SRA = 1;
    };

    //Setting Plt Radio Officer SW radios
    if (_strPlayer in ["PLTOFF"]) then {
        _SR1 = "30.1";
        _SR2 = "30";
        _SR3 = "31";
        _SR4 = "32";
        _SR5 = "33";
        _SR6 = "34";
        _SR7 = "35";
        _SR8 = "36";
        _LR1 = "30";
        _LR2 = "31";
        _LR3 = "32";
        _LR4 = "33";
        _LR5 = "34";
        _LR6 = "35";
        _LR7 = "36";
        _LR8 = "37";
        _LRA = 0;
        _SRA = 1;
    };


    //Setting Plt FAC SW radios
    if (_strPlayer in ["PLTFAC"]) then {
        _SR1 = "30.1";
        _SR2 = "60";
        _SR3 = "66";
        _LR1 = "60";
        _LR2 = "30";
        _LRA = 1;
    };


    //Setting Sniper Team SW radios
    if (_strPlayer in ["Sniper1", "Sniper2"]) then {
        _SR1 = "125.5";
        _SR2 = "130";
        _SR3 = "66";
        _LRA = 0;
        _SRA = 1;
    };


    //Setting Phantoms  SW radios
    if (_strPlayer in ["Ph1","Ph2","Ph3","Ph4","Ph5"]) then {
        _SR1 = "60.1";
        _SR2 = "60";
        _LR1 = "60";
        _LR2 = "60.1";
        _SRA = 1;
        _LRA = 1;
    };


    //Setting Reapers/Talons SW radios
    if (_strPlayer in ["Rpr11","Rpr12","Rpr21","Rpr22","Talon1","Talon2","Talon3"]) then {
        _SR1 = "60";
        _SR2 = "60.1";
        _LR1 = "60";
        _LR2 = "60.1";
        _SRA = 1;
        _LRA = 1;
    };

    //Setting Admin Slots
    if (_strPlayer in ["Admin1","Admin2","Admin3","Admin4"]) then {
        _SR1 = "31";
        _SR2 = "31.1";
        _SR3 = "31.2";
        _SR4 = "32";
        _SR5 = "32.1";
        _SR6 = "33.2";
        _SR7 = "34";
        _SR8 = "34.1";
        _LR1 = "30";
        _LR2 = "60";
        _LR3 = "70";
        _LR4 = "31";
        _LR5 = "32";
        _LR6 = "33";
        _LRA = 0;

    };

    [
        _onlyLR,
        [_SR1, _SR2, _SR3, _SR4, _SR5, _SR6, _SR7, _SR8],
        _SRA,
        [_LR1, _LR2, _LR3, _LR4, _LR5, _LR6, _LR7, _LR8, _LR9],
        _LRA
    ]call FUNC(setFrequencies);
    hintSilent "Platoon Net Programmed";
    nil
};

DFUNC(tfarSettingsSimple) = {
    private _onlyLR = false;
    //frequencies
    //blufor
    private ["_SR1","_SR2","_SR3","_SR4","_SR5","_SR6","_SR7","_SR8","_LR1","_LR2","_LR3","_LR4","_LR5","_LR6","_LR7","_LR8","_LR9"];
    private _SR1 = "30";
    private _SR2 = "31";
    private _SR3 = "32";
    private _SR4 = "33";
    private _SR5 = "34";
    private _SR6 = "35";
    private _SR7 = "36";
    private _SR8 = "37";
    private _LR1 = "30";
    private _LR2 = "60";
    private _LR3 = "70";
    private _LR4 = "31";
    private _LR5 = "32";
    private _LR6 = "33";
    private _LR7 = "34";
    private _LR8 = "35";
    private _LR9 = "36";

    //Setting Simple radios
    [
        _onlyLR,
        [_SR1, _SR2, _SR3, _SR4, _SR5, _SR6, _SR7, _SR8],
        -1,
        [_LR1, _LR2, _LR3, _LR4, _LR5, _LR6, _LR7, _LR8, _LR9],
        -1
    ] call FUNC(setFrequencies);
    hintSilent "Basic Net Programmed";
};

DFUNC(setFrequencies) = {

    params ["_onlyLR", "_SR", "_SRA", "_LR", "_LRA"];

    if (call TFAR_fnc_haveSwRadio && !_onlyLR) then {
        {
            [(call TFAR_fnc_activeSwRadio), _forEachIndex + 1, _x] call TFAR_fnc_SetChannelFrequency;
        } forEach _SR;
        if (_SRA != -1) then {
            [call TFAR_fnc_activeSwRadio, _SRA] call TFAR_fnc_setAdditionalSwChannel;
        };
    };

    if (call TFAR_fnc_haveLRRadio) then {
        {
            [(call TFAR_fnc_activeLrRadio), _forEachIndex + 1, _x] call TFAR_fnc_SetChannelFrequency;
        } forEach _LR;
        if !(_LRA != -1) then {
            [(call TFAR_fnc_activeLrRadio) select 0, (call TFAR_fnc_activeLrRadio) select 1, _LRA] call TFAR_fnc_setAdditionalLrChannel;
        };
    };
};

//[missionNamespace, "arsenalClosed", { true call FUNC(tfarSettings); }] call BIS_fnc_addScriptedEventHandler;
["JK_AssignTFARFrequenzes", "OnRadiosReceived", { false call FUNC(tfarSettings); }, CLib_Player] call TFAR_fnc_addEventHandler;

["playerChanged", {
    QGVAR(restBaseDistance) call CFUNC(localEvent);
}] call CFUNC(addEventhandler);

GVAR(currentState) = 0;

DFUNC(callTFARSettings) = {
    false call ([FUNC(tfarSettings), FUNC(tfarSettingsSimple)] select GVAR(currentState));
    GVAR(currentState) = (GVAR(currentState) + 1) mod 2;
    if (GVAR(currentState) == 0) exitWith {};
    [{
        GVAR(currentState) = 0;
    }, 2] call CFUNC(wait);
    playSound "ClickSoft";
    nil
};

#include "\a3\editor_f\Data\Scripts\dikCodes.h"
[
    "BSO Coop",
    "Set_TFARChannel",
    "Set TFAR Channel Frequencies",
    {
        call FUNC(callTFARSettings);
    }, "", [DIK_END, [false, false, false]]
] call CBA_fnc_addKeybind;
[
    "Program Radio",
    CLib_Player,
    0,
    {
        [QGVAR(baseDistance), EFUNC(Common,isNearBase), [], 30, QGVAR(restBaseDistance)] call CFUNC(cachedCall);
    },
    {
        call FUNC(callTFARSettings);
    }, ["showWindow", false, "ignoredCanInteractConditions", ["isNotInVehicle"]]
] call CFUNC(addAction);
/* Disable is Build in to TFAR 1.0 Beta
["TFAR_AI_Detection", "OnSpeak", {
    params ["_unit", "_isSpeaking"];
    if ((vehicle _unit) call TFAR_fnc_isVehicleIsolated || {isNil "_unit"} || {!alive _unit} || {!_isSpeaking}) exitWith {};

    private _nearHostiles = _unit nearEntities [["Car", "Motorcycle", "Tank","CAManBase","Man"], TF_speak_volume_meters];
    private _onMove = false;
    {
        if (!((vehicle _x) call TFAR_fnc_isVehicleIsolated) && {!isPlayer _x}) then {
            private _value = ((_x knowsAbout _unit) + (TF_speak_volume_meters - (_x distance _unit)) / 6) min 4;
            ["reveal", _x, [_x, _unit, _value]] call CFUNC(targetEvent);
            if (!_onMove && random 4 <= _value) then {
                ["requestMove", _x, [_x, getPos _unit]] call CFUNC(targetEvent);
                _onMove = true;
            };
            DUMP("new Reveal Value:" + str _value);
        };
        nil
    } count _nearHostiles;

}, CLib_Player] call TFAR_fnc_addEventHandler;
*/

/*

DFUNC(checkEnforced) = {
    if !(alive player) exitWith {};
    if (player in (call BIS_fnc_listCuratorPlayers)) exitWith {};
    if !(GVAR(forceTFAR)) exitWith {};
    private _isConnected = call TFAR_fnc_isTeamSpeakPluginEnabled;
    if !(_isConnected) then {
        "TFAR is Enforced" hintC [
            "Start your TFAR Plugin",
            "Join the Correct Server",
            "and Correct Channel",
            "Dickhead"
        ];
    };
};


["missionStarted", {
    call FUNC(checkEnforced);
}] call CFUNC(addEventhandler);

[{call FUNC(checkEnforced)}, 2] call CFUNC(addPerFrameHandler);


if (call EFUNC(Common,AchillesLoaded)) then {
    ["BSO Coop", "Change Force TFAR", {
        private _dialog_result = ["Force TFAR",
            [
                ["New Force Status", ["Enabled", "Disabled"], 0]
            ]
        ] call Ares_fnc_showChooseDialog;
        if (_dialog_result isEqualTo []) exitWith {};

        private _result = _dialog_result select 0;
        GVAR(forceTFAR) = (_result isEqualTo 0);
        publicVariable QGVAR(forceTFAR);
    }] call Ares_fnc_RegisterCustomModule;
};

*/
