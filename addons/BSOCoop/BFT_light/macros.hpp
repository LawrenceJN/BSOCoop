#define MODULE BFT_light
#include "\bso\BSOCoop\addons\BSOCoop\macros.hpp"
#define CFGBFT(X) QUOTE(PREFIX/CfgBFT/X)

#define GETSIDE(var) (var getVariable [QGVAR(playerSide), side var])
