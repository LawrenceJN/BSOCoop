#include "macros.hpp"
/*
    BSO Coop

    Author: joko // Jonas, BadGuy

    Description:
    initilize BFT System

    Parameter(s):
    None<

    Returns:
    None
*/

params ["_unit"];
if (!(isPlayer _unit) && GVAR(onlyPlayer)) exitWith { false };
if (_unit isKindOf "Logic") exitWith { false }; // Dont Draw Logics at all
!isNull _unit
 && alive _unit
 && GETSIDE(_unit) == GETSIDE(player)
 && simulationEnabled _unit
 && !isObjectHidden _unit;
