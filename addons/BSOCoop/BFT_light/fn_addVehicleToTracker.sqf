#include "macros.hpp"
/*
    BSO Coop

    Author: joko // Jonas, BadGuy

    Description:
    Add Group Icon to a Group

    Parameter(s):
    None

    Returns:
    None
*/
#define DEFAULT_ICON "\A3\ui_f\data\map\vehicleicons\iconcar_ca.paa"
params ["_vehicleIconId", "_vehicle"];
private _crew = crew _vehicle;
private _color = call {
    if (player in _crew) exitWith {
        [1, 0.4, 0, 1]
    };
    private _inGroup = {
        ({group _x isEqualTo (group player)} count _crew) > 0;
    } count _crew;
    _inGroup = _inGroup > 0;
    if (_inGroup) exitWith {
        [0.13, 0.54, 0.21, 1]
    };
    [0, 0.4, 0.8, 1]
};

private _vehicleMapIcon = [(configFile >> "CfgVehicles" >> typeOf _vehicle >> "Icon"), DEFAULT_ICON, true] call CFUNC(getConfigDataCached);

private _crewCount = ((count _crew) - 1);
private _driver = (driver _vehicle);
if (isNull _driver) then {
    _driver = (crew _vehicle) select 0;
};
private _driverName = name _driver;

private _text = format [["%1", "%1 +%2"] select (_crewCount min 1), _driverName, _crewCount];
private _normalIcon = ["ICON", _vehicleMapIcon, _color, _vehicle, 30, 30, _vehicle, "", 1];
private _hoverIcon = ["ICON", "a3\ui_f\data\Map\Markers\System\dummy_ca.paa", [1, 1, 1, 1], _vehicle, 30, 30, _vehicle, _text, 1];
[
    _vehicleIconId,
    [
        _normalIcon
    ]
] call CFUNC(addMapGraphicsGroup);

[_vehicleIconId, [_normalIcon, _hoverIcon], "hover"] call CFUNC(addMapGraphicsGroup);
