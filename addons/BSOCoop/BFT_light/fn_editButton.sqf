#include "macros.hpp"
/*
    BSO Coop

    Author: joko // Jonas, BadGuy

    Description:
    Edit button Event Action

    Parameter(s):
    None

    Returns:
    None
*/

disableSerialization;
with uiNamespace do {
    private _defaultIcon = GETSIDE(player) call FUNC(getDefaultIcon);
    if (ctrlShown GVAR(ctrlGroup)) then {
        GVAR(ctrlGroup) ctrlShow false;
        GVAR(editButton) ctrlSetText "EDIT";
        (group player) setGroupIdGlobal [ctrlText GVAR(tbName)];
        (group player) setVariable [QGVAR(icon), (GVAR(lbIcon) lbData lbCurSel GVAR(lbIcon)), true];
        (group player) setVariable [QGVAR(radioSR), ctrlText GVAR(tbRadioSR), true];
        (group player) setVariable [QGVAR(radioLR), ctrlText GVAR(tbRadioLR), true];
        (group player) setVariable [QGVAR(remarks), ctrlText GVAR(tbRemarks), true];

        GVAR(ctrlGroup) ctrlCommit 0;
        GVAR(editButton) ctrlCommit 0;
        with missionNamespace do {
            [{
                call FUNC(updateGroupMarkers);
                QGVAR(updateGroupIcons) call CFUNC(globalEvent);
            }] call CFUNC(execNextFrame);
        };
    } else {
        GVAR(ctrlGroup) ctrlShow true;
        GVAR(editButton) ctrlSetText "SAVE";
        lbClear GVAR(lbIcon);

        {
            private _data = GVAR(iconNamespace) getVariable _x;

            _data params ["_icon", "_color", "", "_visName", "_side"];
            if (_side isEqualTo GETSIDE(player)) then {
                private _tmp = GVAR(lbIcon) lbAdd _visName;
                GVAR(lbIcon) lbSetPicture [_tmp, _icon];
                GVAR(lbIcon) lbSetPictureColor [_tmp, _color];
                GVAR(lbIcon) lbSetData [_tmp, _x];

                if (_x == ((group player) getVariable [QGVAR(icon), _defaultIcon])) then {
                    GVAR(lbIcon) lbSetCurSel _tmp;
                };
            };
            nil
        } count ([GVAR(iconNamespace), QGVAR(allMarkerNames)] call CFUNC(allVariables));

        GVAR(lbIcon) ctrlCommit 0;

        GVAR(tbName) ctrlSetText (groupId (group player));
        GVAR(tbName) ctrlCommit 0;

        GVAR(tbRemarks) ctrlSetText ((group player) getVariable [QGVAR(remarks), ""]);
        GVAR(tbRemarks) ctrlCommit 0;

        GVAR(tbRadioSR) ctrlSetText ((group player) getVariable [QGVAR(radioSR), "n/a"]);
        GVAR(tbRadioSR) ctrlCommit 0;

        GVAR(tbRadioLR) ctrlSetText ((group player) getVariable [QGVAR(radioLR), "n/a"]);
        GVAR(tbRadioLR) ctrlCommit 0;

        GVAR(ctrlGroup) ctrlCommit 0;
        GVAR(editButton) ctrlCommit 0;
    };
};
