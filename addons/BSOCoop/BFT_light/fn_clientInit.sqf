#include "macros.hpp"
/*
    BSO Coop

    Author: joko // Jonas, BadGuy

    Description:
    initilize BFT System

    Parameter(s):
    None

    Returns:
    None
*/

GVAR(onlyPlayer) = ([CFGBFT(onlyPlayer), 0] call CFUNC(getSetting)) == 1;
GVAR(showGroupPlayer) = ([CFGBFT(showGroupPlayer), 1] call CFUNC(getSetting)) == 1;
GVAR(drawLinesServer) = ([CFGBFT(drawLines), 1] call CFUNC(getSetting)) == 1;
GVAR(useBSOMarker) = ([CFGBFT(useBSOMarker), 0] call CFUNC(getSetting)) == 1;
GVAR(onlyGroupLeaderGroupMarker) = ([CFGBFT(onlyGroupLeaderGroupMarker), 1] call CFUNC(getSetting)) == 1;
GVAR(onlyGroupLeaderUnitMarker) = ([CFGBFT(onlyGroupLeaderUnitMarker), 1] call CFUNC(getSetting)) == 1;
GVAR(iconNamespace) = false call CFUNC(createNamespace);

uiNamespace setVariable [QGVAR(iconNamespace), GVAR(iconNamespace)];
GVAR(allIcons) = [[], [], []];
GVAR(updateGroupIcons) = false;
GVAR(updatePlayerIcons) = false;

DFUNC(updatePlayerSide) = {
    params [["_new", objNull, [objNull]], ["_old", objNull, [objNull]]];
    if !(isNull _new) then {
        [_new, QGVAR(playerSide), playerSide, 1] call CFUNC(setVariablePublic);
    };
    if !(isNull _old) then {
        [_old, QGVAR(playerSide), sideEmpty, 1] call CFUNC(setVariablePublic);
    };
};

player call FUNC(updatePlayerSide);

DFUNC(updateLeader) = {
    GVAR(showEditButton) = false;
    if (leader player == player) then {
        {
            _x setVariable [QGVAR(isLeader), false, true];
            nil
        } count (units player);
        GVAR(showEditButton) = true;
        player setVariable [QGVAR(isLeader), true, true];
        [QGVAR(updateGroupIcons), side player] call CFUNC(targetEvent);
    };

    with uiNamespace do {
        if !(isNil QGVAR(editButton)) then {
            GVAR(editButton) ctrlShow (missionNamespace getVariable [QGVAR(showEditButton), false]);
            GVAR(editButton) ctrlCommit 0;
        };

    };
};

private _types = [
    ["NATO_BLUFOR", "NATO_OPFOR", "NATO_Independent"],
    ["BSO_BLUFOR", "BSO_OPFOR", "BSO_Independent", "BSO_Civilian"]
] select GVAR(useBSOMarker);

private _classes = format ["getText (_x >> 'markerClass') in %1",_types] configClasses (configfile >> "CfgMarkers");

// add Fallback Marker
private _config = configFile >> "CfgMarkers" >> "hd_unknown";
private _data = [
    getText (_config >> "icon"),
    [0, 0, 0, 1],
    getNumber (_config >> "size"),
    getText (_config >> "name"),
    sideUnknown
];

[GVAR(iconNamespace), "hd_unknown", _data, QGVAR(allMarkerNames)] call CFUNC(setVariable);
{
    private _varName = configName _x;
    private _data = [
        getText (_x >> "icon"),
        (_x >> "color") call BIS_fnc_colorConfigToRGBA,
        getNumber (_x >> "size"),
        getText (_x >> "name"),
        switch (getText (_x >> "markerClass")) do {
            case (_types select 0): {
                west
            };
            case (_types select 1): {
                east
            };
            case (_types select 2): {
                independent
            };
            default {
                civilian
            };
        }
    ];
    [GVAR(iconNamespace), _varName, _data, QGVAR(allMarkerNames)] call CFUNC(setVariable);
    nil
} count _classes;

if !(GVAR(useBSOMarker)) then {
    // add Marker for Civs because there is non for Vanilla
    private _config = configFile >> "CfgMarkers" >> "c_unknown";
    private _data = [
        getText (_config >> "icon"),
        (configFile >> "CfgMarkerColors" >> "ColorCIV" >> "color") call BIS_fnc_colorConfigToRGBA,
        getNumber (_config >> "size"),
        getText (_config >> "name"),
        civilian
    ];
    [GVAR(iconNamespace), "c_unknown", _data, QGVAR(allMarkerNames)] call CFUNC(setVariable);
};

[QGVAR(updateGroupIcons), {
    if (GVAR(onlyGroupLeaderUnitMarker) && (leader CLib_Player) != CLib_player) exitWith {};
    if (GVAR(updateGroupIcons)) exitWith {};
    GVAR(updateGroupIcons) = true;
    [{call FUNC(updateGroupMarkers); GVAR(updateGroupIcons) = false; }, 0.3] call CFUNC(wait);
}] call CFUNC(addEventhandler);

[QGVAR(updatePlayerIcons), {
    if (GVAR(onlyGroupLeaderGroupMarker) && (leader CLib_Player) != CLib_player) exitWith {};
    if (GVAR(updatePlayerIcons)) exitWith {};
    GVAR(updatePlayerIcons) = true;
    [{call FUNC(updatePlayerMarkers); GVAR(updatePlayerIcons) = false; }, 0.3] call CFUNC(wait);
}] call CFUNC(addEventhandler);


["sideChanged", {
    player call FUNC(updatePlayerSide);
    QGVAR(updateGroupIcons) call CFUNC(globalEvent);
    QGVAR(updatePlayerIcons) call CFUNC(globalEvent);
}] call CFUNC(addEventhandler);

["visibleMapChanged", {
    (_this select 0) params ["_mapOn"];

    if (_mapOn) then {
        call FUNC(updateGroupMarkers);
        call FUNC(updatePlayerMarkers);
    } else {
        with uiNamespace do {
            GVAR(ctrlGroup) ctrlShow false;
            GVAR(ctrlGroup) ctrlCommit 0;
            GVAR(editButton) ctrlSetText "EDIT";
            GVAR(groupToolTip) ctrlShow false;
            GVAR(groupToolTip) ctrlCommit 0;
        };
    };
    call FUNC(onHoverOut);
}] call CFUNC(addEventhandler);


["visibleGPSChanged", {
    (_this select 0) params ["_mapOn"];

    call FUNC(updateGroupMarkers);
    call FUNC(updatePlayerMarkers);
    call FUNC(onHoverOut);
}] call CFUNC(addEventhandler);

["groupChanged", {
    (_this select 0) params ["_new", "_old"];
    private _targets = units _new;
    _targets append (units _old);
    _targets = _targets arrayIntersect _targets;
    [QGVAR(updatePlayerIcons), _targets] call CFUNC(targetEvent);
    player call FUNC(updatePlayerSide);
}] call CFUNC(addEventhandler);

["groupUnitsChanged", {
    (_this select 0) params ["_new", "_old"];
    private _targets = +_new;
    _targets append _old;
    _targets = _targets arrayIntersect _targets;
    [QGVAR(updatePlayerIcons), _targets] call CFUNC(targetEvent);
}] call CFUNC(addEventhandler);

["assignedTeamChanged", {
    (_this select 0) params ["_new"];
    player setVariable [QGVAR(assignedTeam), _new, true];
    [QGVAR(updatePlayerIcons), group player] call CFUNC(targetEvent);
}] call CFUNC(addEventhandler);

["playerChanged", {
    (_this select 0) params ["_new", "_old"];
    if (_old getVariable [QGVAR(isLeader), false]) then {
        [QGVAR(updateGroupIcons), [side _new, side _old]] call CFUNC(targetEvent);
    };
    [QGVAR(updatePlayerIcons), units _new] call CFUNC(targetEvent);

    [_new, _old] call FUNC(updatePlayerSide);
}] call CFUNC(addEventhandler);

["Respawn", {
    (_this select 0) params ["_new", "_old"];
    if (_old getVariable [QGVAR(isLeader), false]) then {
        [QGVAR(updateGroupIcons), [side _new, side _old]] call CFUNC(targetEvent);
    };
    [QGVAR(updatePlayerIcons), units _new] call CFUNC(targetEvent);
    [_new, _old] call FUNC(updatePlayerSide);
}] call CFUNC(addEventhandler);

["Killed", {
    (_this select 0) params ["_unit"];
    if (_unit getVariable [QGVAR(isLeader), false]) then {
        [QGVAR(updateGroupIcons), side _unit] call CFUNC(targetEvent);
    };
    [objNull, _unit] call FUNC(updatePlayerSide);
    [QGVAR(updatePlayerIcons), units _unit] call CFUNC(targetEvent);
}] call CFUNC(addEventhandler);

[{
    call FUNC(updateLeader);
}, 2] call CFUNC(wait);
["leaderChanged", FUNC(updateLeader)] call CFUNC(addEventHandler);

["vehicleChanged", {
    if (player getVariable [QGVAR(isLeader), false]) then {
        [QGVAR(updateGroupIcons), side player] call CFUNC(targetEvent);
    };
    [QGVAR(updatePlayerIcons), units player] call CFUNC(targetEvent);
}] call CFUNC(addEventhandler);

[FUNC(bftdialog), {
    !isNull ((findDisplay 12) displayCtrl 51)
}] call CFUNC(waitUntil);

QGVAR(updateGroupIcons) call CFUNC(localEvent);
QGVAR(updatePlayerIcons) call CFUNC(localEvent);

GVAR(drawLinesClient) = false;

if (GVAR(drawLinesServer)) then {
    [
        QGVAR(renderLines),
        "CHECKBOX",
        ["Render BFT Lines", "Renders BFT lines between groups"],
        "BSO Coop",
        false,
        nil,
        {
            params ["_value"];
            GVAR(drawLinesClient) = _value;
            QGVAR(updateGroupIcons) call CFUNC(localEvent);
            QGVAR(updatePlayerIcons) call CFUNC(localEvent);
        }
    ] call CBA_Settings_fnc_init;
};
