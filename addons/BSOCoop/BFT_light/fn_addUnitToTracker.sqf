#include "macros.hpp"
/*
    BSO Coop

    Author: joko // Jonas, BadGuy

    Description:
    Update all Player Marker

    Parameter(s):
    None

    Returns:
    None
*/
#define DEFAULT_ICON "\A3\ui_f\data\map\vehicleicons\iconMan_ca.paa"
params ["_iconId", "_unit"];
private _team = _unit getVariable [QGVAR(assignedTeam), "MAIN"];

private _color = if (CLib_player == _unit) then {
    [1, 0.4, 0, 1];
} else {
    [
        [0.62, 0.7, 0.94, 1],
        [1, 0, 0.1, 1],
        [0.1, 1, 0, 1],
        [0.1, 0, 1, 1],
        [1, 1, 0.1, 1]
    ] select ((["MAIN", "RED", "GREEN", "BLUE", "YELLOW"] find ([_team] param [0, "MAIN"])) max 0);
};
private _mapIcon = [(configFile >> "CfgVehicles" >> typeOf _unit >> "Icon"), DEFAULT_ICON, true] call CFUNC(getConfigDataCached);

private _manIcon = ["ICON", _mapIcon, _color, _unit, 20, 20, _unit, "", 1, 0.08, "RobotoCondensed", "right", {
    if (_position getVariable ["ACE_isUnconscious", false]) then {
        _texture = "\A3\ui_f\data\igui\cfg\revive\overlayicons\u100_ca.paa";
        _color = [1, 0, 0, 1];
        _width = 30;
        _height = 30;
        _angle = 0;
    };
}];

private _manDescription = ["ICON", "a3\ui_f\data\Map\Markers\System\dummy_ca.paa", [1, 1, 1, 1], _unit, 22, 22, 0, name _unit, 2];

[_iconId, [_manIcon]] call CFUNC(addMapGraphicsGroup);
[_iconId, [_manIcon, _manDescription], "hover"] call CFUNC(addMapGraphicsGroup);

if ((group _unit) isEqualTo (group player)) then {
    [_iconID + "line", _unit, player] call FUNC(addUnitLineToTracker);
};
