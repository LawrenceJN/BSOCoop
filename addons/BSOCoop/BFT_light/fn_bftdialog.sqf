#include "macros.hpp"
/*
    BSO Coop

    Author: joko // Jonas, BadGuy

    Description:
    Create and Update BFT Dialog

    Parameter(s):
    None

    Returns:
    None
*/

disableSerialization;

with uiNamespace do {
    private _gY = ((((safezoneW / safezoneH) min 1.2) / 1.2) / 25);
    private _gX = (((safezoneW / safezoneH) min 1.2) / 40);

    private _gY0 = SafeZoneY;
    private _gX0 = SafeZoneX;

    private _map = (findDisplay 12);


    GVAR(editButton) = _map ctrlCreate ["RscButton", -1];
    GVAR(editButton) ctrlSetPosition [_gX0 + safeZoneW - 27 * _gX, _gY0 + 0.1 * _gY, 2.5 * _gX, 1.3 * _gY];
    GVAR(editButton) ctrlSetText "EDIT";

    GVAR(ctrlGroup)  = _map ctrlCreate ["RscControlsGroupNoScrollbars", -1];
    GVAR(ctrlGroup) ctrlSetPosition [_gX0 + safeZoneW - 27 * _gX, _gY0 + 1.4 * _gY, 13 * _gX, 8.1 * _gY];
    GVAR(ctrlGroup) ctrlCommit 0;

    private _ctrl = _map ctrlCreate ["RscBackground", -1, GVAR(ctrlGroup)];
    _ctrl ctrlSetPosition [0, 0, 13 * _gX, 8.1 * _gY];
    _ctrl ctrlSetBackgroundColor [0, 0, 0, 0.8];
    _ctrl ctrlCommit 0;

    _ctrl = _map ctrlCreate ["RscText", -1, GVAR(ctrlGroup)];
    _ctrl ctrlSetPosition [0.1 * _gX, 0.1 * _gY, 4.8 * _gX, 1 * _gY];
    _ctrl ctrlSetText "Group Name:";
    _ctrl ctrlCommit 0;

    GVAR(tbName) = _map ctrlCreate ["RscEdit", -1, GVAR(ctrlGroup)];
    GVAR(tbName) ctrlSetPosition [5 * _gX, 0.2 * _gY, 7.9 * _gX, 1 * _gY];
    GVAR(tbName) ctrlCommit 0;

    GVAR(lbIcon) = _map ctrlCreate ["RscListBox", -1, GVAR(ctrlGroup)];
    GVAR(lbIcon) ctrlSetPosition [0.2 * _gX, 1.4 * _gY, 12.6 * _gX, 4.4 * _gY];
    GVAR(lbIcon) ctrlCommit 0;

    _ctrl = _map ctrlCreate ["RscText", -1, GVAR(ctrlGroup)];
    _ctrl ctrlSetPosition [0.1 * _gX, 5.8 * _gY, 2.4 * _gX, 1 * _gY];
    _ctrl ctrlSetText "SR:";
    _ctrl ctrlCommit 0;

    GVAR(tbRadioSR) = _map ctrlCreate ["RscEdit", -1, GVAR(ctrlGroup)];
    GVAR(tbRadioSR) ctrlSetPosition [2.5 * _gX, 5.8 * _gY, 4 * _gX, 1 * _gY];
    GVAR(tbRadioSR) ctrlCommit 0;

    _ctrl = _map ctrlCreate ["RscText", -1, GVAR(ctrlGroup)];
    _ctrl ctrlSetPosition [6.5 * _gX, 5.8 * _gY, 2.4 * _gX, 1 * _gY];
    _ctrl ctrlSetText "LR:";
    _ctrl ctrlCommit 0;

    GVAR(tbRadioLR) = _map ctrlCreate ["RscEdit", -1, GVAR(ctrlGroup)];
    GVAR(tbRadioLR) ctrlSetPosition [8.9 * _gX, 5.8 * _gY, 4 * _gX, 1 * _gY];
    GVAR(tbRadioLR) ctrlCommit 0;

    _ctrl = _map ctrlCreate ["RscText", -1, GVAR(ctrlGroup)];
    _ctrl ctrlSetPosition [0.1 * _gX, 6.9 * _gY, 4.8 * _gX, 1 * _gY];
    _ctrl ctrlSetText "Remarks:";
    _ctrl ctrlCommit 0;

    GVAR(tbRemarks) = _map ctrlCreate ["RscEdit", -1, GVAR(ctrlGroup)];
    GVAR(tbRemarks) ctrlSetPosition [5 * _gX, 6.9 * _gY, 7.9 * _gX, 1 * _gY];
    GVAR(tbRemarks) ctrlCommit 0;



    GVAR(ctrlGroup) ctrlShow false;
    GVAR(ctrlGroup) ctrlCommit 0;


    GVAR(groupToolTip)  = _map ctrlCreate ["RscControlsGroupNoScrollbars",-1];
    GVAR(groupToolTip) ctrlSetPosition [0, 0, 8 * _gX, 2.3 * _gY];

    _ctrl = _map ctrlCreate ["RscBackground", -1, GVAR(groupToolTip)];
    _ctrl ctrlSetPosition [0, 0, 8 * _gX, 2.2 * _gY];
    _ctrl ctrlSetBackgroundColor [0, 0, 0, 0.8];
    _ctrl ctrlCommit 0;

    GVAR(ttRadio) = _map ctrlCreate ["RscText", -1, GVAR(groupToolTip)];
    GVAR(ttRadio) ctrlSetPosition [0.1 * _gX, 0.1 * _gY, 7.8 * _gX, 1 * _gY];
    GVAR(ttRadio) ctrlSetText "Radio";
    GVAR(ttRadio) ctrlCommit 0;

    GVAR(ttRemarks) = _map ctrlCreate ["RscText", -1, GVAR(groupToolTip)];
    GVAR(ttRemarks) ctrlSetPosition [0.1 * _gX, 1 * _gY, 7.8 * _gX, 1 * _gY];
    GVAR(ttRemarks) ctrlSetText "Remarks";
    GVAR(ttRemarks) ctrlCommit 0;

    GVAR(groupToolTip) ctrlShow false;
    GVAR(groupToolTip) ctrlCommit 0;

    GVAR(editButton) ctrlAddEventHandler ["ButtonClick", FUNC(editButton)];
    GVAR(editButton) ctrlShow (missionNamespace getVariable [QGVAR(showEditButton), false]);
    GVAR(editButton) ctrlCommit 0;

};
