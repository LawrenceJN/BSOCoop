#include "macros.hpp"
/*
    BSO Coop

    Author: joko // Jonas, BadGuy

    Description:
    Update all Player Marker

    Parameter(s):
    None

    Returns:
    None
*/
params ["_unit"];
with missionNamespace do {
    if (!(GVAR(showGroupPlayer)) || (GVAR(onlyGroupLeaderUnitMarker) && leader CLib_player == CLib_player)) exitWith {};
    private _allUnits = [];
    {
        if (_x call FUNC(isValidUnit)) then {
            if (isNull objectParent _x) then {
                private _iconID = format [QGVAR(unit_%1_%2), _x, group _x];
                [_iconID, _x] call FUNC(addUnitToTracker);
                _allUnits pushBack _iconID;
                if !((group _x) isEqualTo (group player)) then {
                    [_iconID + "line", _x, leader _x] call FUNC(addUnitLineToTracker);
                };
            } else {
                private _vehicle = objectParent _x;
                private _iconID = format [QGVAR(vehicle_%1), _vehicle];
                [_iconID, _vehicle] call FUNC(addVehicleToTracker);
                _allUnits pushBack _iconID;
            };
        };
        nil
    } count (units _unit);
    GVAR(allIcons) set [2, _allUnits];
    DUMP("Update External Group Icons")
};
