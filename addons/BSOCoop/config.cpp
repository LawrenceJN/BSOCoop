#include "macros.hpp"
class CfgPatches {
    class BSOCoop {
        units[] = {};
        weapons[] = {};
        requiredVersion = 1.56;
        author = "joko // Jonas";
        authors[] = {"joko // Jonas"};
        authorUrl = "https://twitter.com/joko48Ger";
        version = VERSION;
        versionStr = QUOTE(VERSION);
        versionAr[] = {VERSION_AR};
        requiredAddons[] = {"CLib"};
    };
};

#include "CfgCLibModules.hpp"

class BSO {
    class CfgCommon {
        enableDynamicGroups = 0;
        speedModifier = 1;
        infiniteStamina = 0;
    };
    class CfgArtillery {
        usedItems[] = {"ACE_Vector", "Rangefinder", "Laserdesignator", "Binocular"};
    };
    class CfgBFT {
        onlyPlayer = 1;
        showGroupPlayer = 0;
        drawLines = 1;
        useBSOMarker = 0;
        onlyGroupLeaderGroupMarker = 1;
        onlyGroupLeaderUnitMarker = 1;
    };

    class CfgMedicBuildings {
        enabled = 0;
        healTimePerUnit = 5;
        healCooldownPerUnit = 180;
        buildings[] = {"Land_Medevac_house_V1_F", "Land_Medevac_HQ_V1_F"};
    };
    class CfgSpectatorRespawn {
        enabled = 0;
        waitTime = 100;
        allowedSides[] = {};
        paramters[] = {1, 1, 1, 1, 1, 1, 1, 1};
        ignoreList[] = {"76561198024742337"};
    };
    class CfgTFAR {
        forceTFAR = 1;
    };
    class CfgCaching {
        DistanceGroup = "2000";
        DistanceVehicle = "2500";
        DistanceEmptyVehicle = "1000";
        DistanceProps = "500";
        CoefDefault = "2.2";
    };
    class CfgScoreBoard {
        enableFix = 1;
        usePersistentScoreBoard = 0;
    };
    class CfgWhitelist {
        class Units {
            unit123[] = {"Admins"};
        };
        class PlayerGroups {
            Admins[] =  {"76561198024742337"};
        };
    };
};

class CfgCLibSettings {
    bso_common[] = {"BSO", "CfgCommon"};
    bso_bft[] = {"BSO", "CfgBFT"};
    bso_artillery[] = {"BSO", "CfgArtillery"};
    bso_MedicBuildings[] = {"BSO", "CfgMedicBuildings"};
    bso_spectatorrespawn[] = {"BSO", "CfgSpectatorRespawn"};
    bso_tfar[] = {"BSO", "CfgTFAR"};
    bso_Caching[] = {"BSO", "CfgCaching"};
    bso_ScoreBoard[] = {"BSO", "CfgScoreBoard"};
    bso_Servicing[] = {"BSO", "CfgServicing"};
    bso_Whitelist[] = {"BSO", "CfgWhitelist"};
};
