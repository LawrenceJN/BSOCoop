#include "macros.hpp"
/*
    BSO Coop

    Author: joko // Jonas

    Description:


    Parameter(s):
    None

    Returns:
    None
*/
[QGVAR(requestAriFire), {
    (_this select 0) params ["_vehicle", "_pos", "_type", ["_rounds", 3], "_requester"];
    if (isNil "_type") then {
        _type = (getArtilleryAmmo [_vehicle]) select 0;
    };
    private _eta = _vehicle getArtilleryETA [_pos, _type ];
    if (_eta == -1) exitWith {
        [QGVAR(cantHitTarget), _requester] call CFUNC(targetEvent);
    };
    ["doArtilleryFire", _vehicle, [_vehicle, _pos, _type, _rounds]] call CFUNC(targetEvent);
    [QGVAR(firedNotification), _requester, _eta, _type, _rounds, _pos] call CFUNC(targetEvent);
}] call CFUNC(addEventhandler);
