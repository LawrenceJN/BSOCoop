#include "macros.hpp"
/*
    BSO Coop

    Author: joko // Jonas

    Description:


    Parameter(s):
    None

    Returns:
    None
*/

["doArtilleryFire", {
    (_this select 0) params ["_vehicle", "_pos", "_type", "_rounds"];
    _vehicle doArtilleryFire [_pos, _type, _rounds];
}] call CFUNC(addEventhandler);
