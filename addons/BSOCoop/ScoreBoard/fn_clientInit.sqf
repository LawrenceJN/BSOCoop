#include "macros.hpp"
/*
    BSO Coop

    Author: Lawrence

    Description:
    initialize persistent scoreboard.

    Parameter(s):
    None

    Returns:
    None
*/

GVAR(usePersistentScoreBoard) = ([CFGSCOREBOARD(usePersistentScoreBoard), 1] call CFUNC(getSetting)) == 1;
if !(GVAR(usePersistentScoreBoard)) exitWith { DUMP("usePersistentScoreBoard = 0"); };
GVAR(isContinuous) = (
    (((toLower serverName) find "arrowhead") > -1)
    || (((toLower serverName) find "public") > -1)
    || (((toLower serverName) find "continuous") > -1)
);

private _profileScores = [];

if (GVAR(isContinuous)) then {
    _profileScores = profileNamespace getVariable [QGVAR(Continuous_persistentScores), [0, 0, 0, 0, 0]];
} else {
    _profileScores = profileNamespace getVariable [QGVAR(persistentScores), [0, 0, 0, 0, 0]];
};

[{
    params ["_profileScores"];
    if !((getPlayerScores CLib_player) isEqualTo [0, 0, 0, 0, 0, 0]) exitWith {};
    ["addPlayerScores", [CLib_player, _profileScores]] call CFUNC(serverEvent);
}, {
    (count (getPlayerScores CLib_player)) > 1
}, [_profileScores]] call CFUNC(waitUntil);

[QGVAR(scoreChanged), {
    [{
        private _scores = getPlayerScores CLib_player;
        if (_scores isEqualTo []) exitWith {};
        _scores deleteAt 5;

        if (GVAR(isContinuous)) then {
            profileNamespace setVariable [QGVAR(Continuous_persistentScores), _scores];
        } else {
            profileNamespace setVariable [QGVAR(persistentScores), _scores];
        };
        saveProfileNamespace;
    }, 1, []] call CFUNC(wait);
}, []] call CFUNC(addEventHandler);
