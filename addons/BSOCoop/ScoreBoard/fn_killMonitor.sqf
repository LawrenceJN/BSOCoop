#include "macros.hpp"
/*
    BSO Coop

    Author: joko // Jonas & Lawrence

    Description:
    initilize kill monitor

    Parameter(s):
    None

    Returns:
    None
*/

["entityCreated", {
    (_this select 0) params ["_obj"];
    private _wrongType = 0;
    {
        if !(_obj isKindOf _x) then {_wrongType = _wrongType + 1};
    } count ["CAManBase", "Helicopter", "Ship", "Car", "Plane", "Tank"];
    if (_wrongType > 5) exitWith {};
    if (isPlayer _obj) exitWith {}; // only trigger if unit is not player
    if !(local _obj) exitWith {}; // only trigger if obj is local
    _obj call FUNC(addKilledEH);
}] call CFUNC(addEventhandler);

[QGVAR(addEventHandlerToUnit), {
    (_this select 0) params ["_unit"];
    _unit call DFUNC(addKilledEH);
}] call CFUNC(addEventhandler);

DFUNC(vehicleScoreType) = {
    params ["_unit"];
    private _car = [["Car", "Ship"], 1];
    private _tank = [["Tank"], 2];
    private _air = [["Air"], 3];
    private _scoreIndex = -1;
    private _exit = false;
    {
        if (_exit) exitWith {};
        _x params ["_types", "_index"];
        {
            if (_unit isKindOf _x) exitWith {
                _scoreIndex = _index;
                _exit = true;
            };
            nil
        } count _types;
        nil
    } count [_tank, _car, _air];
    _scoreIndex
};

DFUNC(addKilledEH) = {
    params ["_obj"];

    if !((_obj getVariable [QGVAR(EHIDs), []]) isEqualTo []) exitWith {};
    private _data = [];
    private _id = _obj addEventHandler ["Local", {
        params ["_obj", "_local"];
        if (_local) exitWith {};
        {
            _obj removeEventHandler _x;
            nil
        } count (_obj getVariable [QGVAR(EHIDs), []]);
        [QGVAR(addEventHandlerToUnit), _obj, _obj] call CFUNC(targetEvent);
    }];

    _data pushback ["Local", _id];

    _id = _obj addEventHandler ["Killed", {
        params ["_unit", "", "_killer"];
        DUMP(typeOf _unit + " died as " + str _unit);
        if  (!isNull _killer && {_killer != _unit} && {isPlayer _killer}) exitWith {
            if !(_unit isKindOf "CAManBase") then {
                if !([side group _unit, side group _killer] call BIS_fnc_sideIsFriendly) exitWith {};
                private _scoreIndex = [_unit] call FUNC(vehicleScoreType);
                private _modify = [0, 1, 2, 3, 0] apply {(if (_x == _scoreIndex) then {1} else {0})};
                ["addPlayerScores", [_killer, _modify]] call CFUNC(serverEvent);
                DUMP("Vehicle crash. Score replaced.");
            };
            DUMP("Non-Ace kill for " + name _killer + " of " + typeOf _unit + " --- " + str _unit);
            [QGVAR(scoreChanged), _killer] call CFUNC(targetEvent);
        };

        private _killerACE = _unit getVariable [QGVAR(lastShooter), objNull];
        if (isNull _killerACE || {_killerACE == _unit} || {!isPlayer _killerACE}) exitWith { DUMP("invalid killer of " + str _unit + " : " + name _killerACE); };
        DUMP("Ace kill for " + name _killerACE + " of " + typeOf _unit + " --- " + str _unit);

        if (_unit isKindOf "CAManBase") exitWith {
            DUMP("Man kill.");
            if ([side group _unit, side group _killerACE] call BIS_fnc_sideIsFriendly) then {
                ["addPlayerScores", [_killerACE, [-1, 0, 0, 0, 0]]] call CFUNC(serverEvent);
                DUMP("Score subtracted.");
            } else {
                if ([side group _unit, side group _killerACE] call BIS_fnc_sideIsEnemy) then {
                    ["addPlayerScores", [_killerACE, [1, 0, 0, 0, 0]]] call CFUNC(serverEvent);
                    DUMP("Score added.");
                } else {
                    DUMP("Something Went Horribly Wrong.");
                };
            };
        };

        private _driver = _unit getVariable [QGVAR(lastDriver), 0];
        if (_driver isEqualTo 0 || {_driver isEqualTo sideUnknown}) exitWith { DUMP("lastDriver for " + str _unit + " is invalid: " + str _driver)};
        DUMP("Vehicle kill of (" + str _unit + ") of " + str _driver + " for (" + name _killerACE + ")");


        if ([_driver, side group _killerACE] call BIS_fnc_sideIsEnemy) then {
            private _scoreIndex = [_unit] call FUNC(vehicleScoreType);
            private _modify = [0, 1, 2, 3, 0] apply {
                parseNumber (_x == _scoreIndex);
            };
            ["addPlayerScores", [_killerACE, _modify]] call CFUNC(serverEvent);
            DUMP("Score added: " + str _modify);
        };

        [QGVAR(scoreChanged), _killerACE] call CFUNC(targetEvent);
    }];

    _data pushback ["Killed", _id];

    _id = _obj addEventHandler ["HandleDamage", {
        params ["_unit","","","_source","","","_instigator"];

        if !(alive _unit) exitWith {};
        if (isNull _instigator || _instigator == _unit || !isPlayer _instigator) then {
            _instigator = _source;
        };
        if (isNull _instigator || _instigator == _unit || !isPlayer _instigator) exitWith {};

        [_unit, QGVAR(lastShooter), _instigator, 1] call CFUNC(setVariablePublic);
        //DUMP("last shooter set to: (" + name _instigator + ") for (  " + str _unit + "  )");

        if (!(_unit isKindOf "CAManBase") && {count crew _unit > 0}) then {
            private _lastDriver = side group ((crew _unit) select 0);
            if (isNil "_lastDriver") exitWith {DUMP("no valid crew: " + str crew _unit)};
            [_unit, QGVAR(lastDriver), _lastDriver, 1] call CFUNC(setVariablePublic);
            //DUMP("last driver set to: (" + str _lastDriver + ") for (  " + str _unit + "  )");
        };
        nil
    }];

    _data pushback ["HandleDamage", _id];

    _obj setVariable [QGVAR(EHIDs), _data];

    /*[
        (str _obj),
        [
            ["ICON", "a3\ui_f\data\Map\Markers\System\dummy_ca.paa", [1, 1, 1, 1], _obj, 8, 8, 0, (getText (configFile >> "CfgVehicles" >> (typeOf _obj) >> "displayName")), 2, 0.05, "PuristaMedium", "center"]
        ]
    ] call CFUNC(addMapGraphicsGroup);*/

};
