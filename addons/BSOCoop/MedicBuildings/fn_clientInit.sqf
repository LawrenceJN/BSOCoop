#include "macros.hpp"
/*
    BSO Coop

    Author: Lawrence

    Description:
    initilize medical buildings module

    Parameter(s):
    None

    Returns:
    None
*/

GVAR(medicalBuildings) = [CFGMEDICBUILDINGS(buildings), []] call CFUNC(getSetting);
GVAR(healTimePerUnit) = [CFGMEDICBUILDINGS(healTimePerUnit), 5] call CFUNC(getSetting);
GVAR(healCooldownPerUnit) = [CFGMEDICBUILDINGS(healCooldownPerUnit),150] call CFUNC(getSetting);
private _enabled = ([CFGMEDICBUILDINGS(enabled), 1] call CFUNC(getSetting)) == 1;

if !(_enabled) exitWith {};

[
    "Heal Patients",
    CLib_player,
    0,
    {
        [QGVAR(inMedicalBuilding), {

            if !(CLib_player isEqualTo vehicle CLib_player) exitWith { false };

            private _intersection = lineIntersectsSurfaces [
                eyePos CLib_Player,
                (eyePos CLib_Player) vectorAdd [0,0,20],
                CLib_player
            ];
            if (_intersection isEqualTo []) exitWith { false };

            _intersection = _intersection select 0 select 2;
            if !([_intersection, GVAR(medicalBuildings)] call CFUNC(isKindOfArray)) exitWith { false };

            GVAR(tent) = _intersection;

            damage GVAR(tent) < 1
        }, _this, 2] call CFUNC(cachedCall);
    },
    {
        private _healCooldown = GVAR(tent) getVariable [QGVAR(cooldown), -9999];

        if (serverTime < _healCooldown) exitWith {
            private _remainingSeconds = floor (_healCooldown - serverTime);

            private _remainingStr = if (_remainingSeconds < 60) then {
                str _remainingSeconds + " seconds."
            } else {
                if (_remainingSeconds < 300 && (_remainingSeconds % 60) != 0) then {
                    str floor (_remainingSeconds / 60) + " minutes " + str floor (_remainingSeconds % 60) + " seconds."
                } else {
                    str ceil (_remainingSeconds / 60) + " minutes."
                };
            };

            [
                format ["<t align='center'>Healing available here in<br />%1</t>", _remainingStr],
                "a3\ui_f\data\igui\cfg\simpletasks\types\heal_ca.paa",
                [1, 1, 1],
                CLib_player,
                2.25
            ] call ace_common_fnc_displayTextPicture;
        };

        private _units = nearestObjects [getPos CLib_player, ["CAManBase"], 50];
        _units = _units select {
            alive _x
             && {
                !((([_x] call ACE_medical_fnc_getBloodPressure) apply { floor _x }) isEqualTo [80, 120])
                ||  { (_x getVariable ["ace_medical_pain", 0]) > 0 }
                || { ((getAllHitPointsDamage _x select 2) call BIS_fnc_arithmeticMean) > 0 }
            }
             && {
                private _intersection = lineIntersectsSurfaces [
                    eyePos _x,
                    (eyePos _x) vectorAdd [0, 0, 20],
                    _x,
                    CLib_player
                ];

                !(_intersection isEqualTo [])
                && { (_intersection select 0 select 2) isEqualTo GVAR(tent) }
            }
        };

        if (_units isEqualTo []) exitWith {
            [
                "No units require healing.",
                "a3\ui_f\data\igui\cfg\simpletasks\types\heal_ca.paa",
                [1, 1, 1],
                CLib_player,
                2.25
            ] call ace_common_fnc_displayTextPicture;
        };

        [CLib_player, "ainvpknlmstpslaywrfldnon_medicother", 0] call ace_common_fnc_doAnimation;
        [
            GVAR(healTimePerUnit) * count _units,
            _units,
            {
                params ["_units"];

                {
                    _x setDamage 0;
                    [CLib_player, _x] call ACE_medical_fnc_treatmentAdvanced_fullHeal;

                    nil
                } count _units;

                GVAR(tent) setVariable [QGVAR(cooldown), serverTime + (GVAR(healCooldownPerUnit) * (count _units)), true];

                [CLib_player, "amovpknlmstpsnonwnondnon", 2] call ace_common_fnc_doAnimation;
            },
            {
                [CLib_player, "amovpknlmstpsnonwnondnon", 2] call ace_common_fnc_doAnimation;
            },
            "Healing Patients...",
            {
                if (animationState CLib_player != "ainvpknlmstpslaywrfldnon_medicother") then {
                    [CLib_player, "ainvpknlmstpslaywrfldnon_medicother", 0] call ace_common_fnc_doAnimation;
                };

                true
            }
        ] call ace_common_fnc_progressBar;
    },
    ["priority", 99, "showWindow", false]
] call CFUNC(addAction);
