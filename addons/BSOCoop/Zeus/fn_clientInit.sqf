#include "macros.hpp"
/*
    BSO Coop

    Author: joko // Jonas

    Description:
    client initilize Zeus System

    Parameter(s):
    None

    Returns:
    None
*/

#include "\a3\editor_f\Data\Scripts\dikCodes.h"
[
    "BSO Coop",
    "Toggle_ZeusFPS",
    "Toggle Zeus FPS",
    {
        if (player in (call BIS_fnc_listCuratorPlayers) && !(isNull curatorCamera)) then {
            call FUNC(ToggleZeusFPS);
        };
        playSound "ClickSoft";
        nil
    }, "", [DIK_F2, [false, false, false]]
] call CBA_fnc_addKeybind;

CLib_Player setVariable [QGVAR(runningVersion), productVersion select 7, true];

[{
    if (GVAR(FrameStatusIsRunning)) then {
        CLib_Player setVariable [QGVAR(currentFPS), diag_fps, true];
    };
}, 0.1] call CFUNC(addPerFrameHandler);

CLib_Player setVariable [QGVAR(currentFPS), diag_fps, true];
GVAR(pfhID) = nil;

DFUNC(ToggleZeusFPS) = {
    if (isNil QGVAR(pfhID)) then {
        GVAR(pfhID) = addMissionEventHandler ["Draw3D", FUNC(draw3D)];
        GVAR(FrameStatusIsRunning) = true;
        publicVariable QGVAR(FrameStatusIsRunning);
    } else {
        removeMissionEventHandler ["Draw3D", GVAR(pfhID)];
        GVAR(pfhID) = nil;
        hintSilent "";
        GVAR(FrameStatusIsRunning) = false;
        publicVariable QGVAR(FrameStatusIsRunning);
    };
};

call FUNC(AchillesModules);

["missionEndRunTime", {
    (_this select 0) call FUNC(missionEndRunTime);
}] call CFUNC(addEventhandler);

["entityCreated", {
    (_this select 0) params ["_unit"];
    if (isNil QGVAR(autoUpdateSkills)) exitWith {};
    if (_unit isKindOf "CAManBase") then {
        if (GVAR(setAISkillGlobal)) then {
            [_unit, GVAR(autoUpdateSkills)] call FUNC(setGlobalAISkill);
        } else {
            if (local _unit) then {
                [_unit, GVAR(autoUpdateSkills)] call FUNC(setGlobalAISkill);
            };
        };
    };
}] call CFUNC(addEventhandler);
