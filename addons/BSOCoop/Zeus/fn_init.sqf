#include "macros.hpp"
/*
    BSO Coop

    Author: joko // Jonas

    Description:
    initilize Zeus System

    Parameter(s):
    None

    Returns:
    None
*/

[QGVAR(setUnitAISkill), {
    (_this select 0) params ["_unit"];
    if !(local _unit) exitWith {
        if (isServer) then {
            [QGVAR(setUnitAISkill), owner _unit, _this] call CFUNC(targetEvent);
        } else {
            [QGVAR(setUnitAISkill), _this] call CFUNC(serverEvent);
        };
    };
    (_this select 0) call FUNC(setGlobalAISkill);
}] call CFUNC(addEventhandler);
