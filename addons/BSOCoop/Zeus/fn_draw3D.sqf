#include "macros.hpp"
/*
    BSO Coop

    Author: joko // Jonas

    Description:
    client initilize Zeus FPS System

    Parameter(s):
    None

    Returns:
    None
*/

if (isNull curatorCamera) exitWith {
    call FUNC(ToggleZeusFPS);
};

private _value = 0;
private _count = {
    private _distance = (positionCameraToWorld [0, 0, 0]) distance _x;
    //if zeus camera is farther than 1200 meters away from the targets the text will not display
    private _fps = _x getVariable [QGVAR(currentFPS), -999];
    // if the FPS counter is not activ on client send targetEvent to enable it.
    if (_distance < 1200) then {
        private _showArrow = _fps < 20;
        private _color = [[1, 1, 1, 0.3], [1, 0, 0, 0.7]] select _showArrow;
        private _size = [0.05, 0.06] select _showArrow;
        private _text = [["%1 Loading", name _x], ["%1 arc: %3 FPS: %2", name _x, _fps, _x getVariable [QGVAR(runningVersion), "None"]]] select (_fps != -999);
        //if the FPS is below 20 it turns red and becomes more visible for zeus to see so they are aware
        drawIcon3D [
            "",
            _color,
            getPos _x,
            1,
            2,
            0,
            format _text,
            0,
            _size,
            "PuristaMedium",
            "center",
            _showArrow
        ];
    };
    if (_fps != -999) then {
        _value = _value + _fps;
        true
    } else {
        false
    };
} count allPlayers;
private _average = _value / _count;
hintSilent format ["Server FPS: %1 \nAverage Framerate %2", GVAR(serverFPS), _average];
