#include "macros.hpp"
/*
    BSO Coop

    Author: joko // Jonas

    Description:
    this Function Only Shows the Mission End Display without ending the mission. that can be used within Missions that dont get cut after the Mission is Done

    Parameter(s):
    None

    Returns:
    None
*/
if (isNil QGVAR(endIsRunning)) then {
    GVAR(endIsRunning) = false;
};
if (GVAR(endIsRunning)) exitWith {};
GVAR(endIsRunning) = true;
_this spawn {
    params [
    ["_win", true, [true]],
    ["_playmusic", true, [true]],
    ["_endType", "", [""]]
    ];
    private _layerNoise = "BIS_fnc_endMission_noise" call bis_fnc_rscLayer;
    private _layerInterlacing = "BIS_fnc_endMission_interlacing" call bis_fnc_rscLayer;
    private _layerStatic = "BIS_fnc_endMission_static" call bis_fnc_rscLayer;
    private _layerEnd = "BIS_fnc_endMission_end" call bis_fnc_rscLayer;

    {
    _x cuttext ["","plain"];
    nil
    } count [_layerNoise,_layerInterlacing,_layerStatic,_layerEnd]; //--- Clear existing effects

    if (_playmusic) then {
    private _musicvolume = musicvolume;
    0.2 fademusic 0;
    sleep 0.2;
    private _musicList = if (isnull curatorcamera) then {
    ["EventTrack02_F_Curator","EventTrack01_F_Curator"]
    } else {
    ["EventTrack02_F_Curator","EventTrack03_F_Curator"]
    };
    playmusic (_musicList select _win);
    0 fademusic _musicvolume;
    sleep 0.4;
    };

    _layerStatic cutrsc ["RscStatic","plain", 1, true];

    sleep 0.3;

    showHud false;
    RscMissionEnd_end = _endType;
    RscMissionEnd_win = _win;
    _layerEnd cutrsc ["RscMissionEnd","plain", 1, true];

    sleep 9;

    RscNoise_color = [1,1,1,0];
    _layerNoise cutrsc ["RscNoise","black", 1, true];
    _layerStatic cutrsc ["RscStatic","plain", 1, true];

    sleep 0.5;

    RscNoise_color = [1,1,1,1];
    _layerInterlacing cutrsc ["RscInterlacing","plain"];
    private _musicvolume = musicvolume;
    private _soundvolume = soundVolume;
    2.5 fadesound 0;
    2.5 fademusic 0;

    sleep 2.5;

    {
    _x cuttext ["","plain", 2, false];
    nil
    } count [_layerNoise,_layerInterlacing,_layerStatic,_layerEnd];

    playMusic "";
    0 fademusic _musicvolume;
    0 fadeSound _soundvolume;
    showHud true;
    GVAR(endIsRunning) = false;
};
nil
