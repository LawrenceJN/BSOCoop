#include "macros.hpp"
/*
    BSO Coop

    Author: joko // Jonas

    Description:
    server initilize Zeus System

    Parameter(s):
    None

    Returns:
    None
*/

[{
     if (GVAR(FrameStatusIsRunning)) then {
          GVAR(serverFPS) = diag_fps;
          publicVariable QGVAR(serverFPS);
     };
}, 1] call CFUNC(addPerFrameHandler);

GVAR(FrameStatusIsRunning) = false;
publicVariable QGVAR(FrameStatusIsRunning);

["entityCreated", {
    (_this select 0) params ["_obj"];
    {
        if (_obj isKindOf "CAManBase") then {
            ["addCuratorEditableObjects", [_x, [[_obj], true]]] call CFUNC(serverEvent);
        };
        nil
    } count allCurators;
}] call CFUNC(addEventhandler);
