#include "macros.hpp"
/*
    BSO Coop

    Author: joko // Jonas

    Description:
    Set Global AI Skill

    Parameter(s):
    0: Unit <Object>
    1: AI Skill Array <Array>

    Returns:
    None
*/
DUMP("setUnitAISkill" + str _this);

params ["_unit", "_skillData"];

if !(local _unit) exitWith {
    [QGVAR(setUnitAISkill), _this] call CFUNC(serverEvent);
};

_skillData params [
    "_general", "_commanding", "_courage", "_aimAcc",
    "_aimShake", "_amimSpeed", "_reloadSpeed",
    "_spotDis", "_spotTime",
    "_cover", "_combat"
];
_unit setSkill ["general", _general];
_unit setSkill ["commanding", _commanding];
_unit setSkill ["courage", _courage];
_unit setSkill ["aimingAccuracy", _aimAcc];
_unit setSkill ["aimingShake", _aimShake];
_unit setSkill ["aimingSpeed", _amimSpeed];
_unit setSkill ["reloadSpeed", _reloadSpeed];
_unit setSkill ["spotDistance", _spotDis];
_unit setSkill ["spotTime", _spotTime];

if (_cover) then {
    _unit enableAI "COVER";
} else {
    _unit disableAI "COVER";
};

if (_combat) then {
    _unit enableAI "AUTOCOMBAT";
} else {
    _unit disableAI "AUTOCOMBAT";
};
