#include "macros.hpp"
/*
    BSO Coop

    Author: joko // Jonas

    Description:


    Parameter(s):
    None

    Returns:
    None
*/
GVAR(pfhID) = nil;


["entityCreated", {
    if (isNil QGVAR(pfhID)) exitWith {};
    (_this select 0) params ["_obj"];
    if (isPlayer _obj) exitWith {}; // only trigger if unit is not player
    if (_obj isKindOf "LaserTarget") exitWith {}; // exclude lasers
    ["enableDynamicSimulation", [_obj, true]] call CFUNC(serverEvent);
}] call CFUNC(addEventhandler);

[QGVAR(toggleLoop), {
    (_this select 0) params ["_enable"];
    if (_enable) then {
        if (isNil QGVAR(pfhID)) then {
            {
                ["enableDynamicSimulation", [_x, true]] call CFUNC(serverEvent);
            } count (entities [[], ["Logic", "LaserTarget"], true, true]);
            GVAR(pfhID) = [{
                {
                    if (!(dynamicSimulationEnabled _x) && !(isPlayer leader _x) && !(_x getVariable [QGVAR(noCache), false])) exitWith {
                        ["enableDynamicSimulation", [_x, true]] call CFUNC(serverEvent);
                    };
                    nil
                } count allGroups;
            }, 5] call CFUNC(addPerFrameHandler);
        };
    } else {
        if !(isNil QGVAR(pfhID)) then {
            GVAR(pfhID) call CFUNC(removePerFrameHandler);
            GVAR(pfhID) = nil;
        };
        {
            ["enableDynamicSimulation", [_x, false]] call CFUNC(serverEvent);
            nil
        } count allGroups;

        {
            ["enableDynamicSimulation", [_x, false]] call CFUNC(serverEvent);
        } count (entities [[], ["Logic", "LaserTarget"], true, true]);
    };
}] call CFUNC(addEventhandler);
