#include "macros.hpp"
/*
    BSO Coop

    Author: joko // Jonas

    Description:


    Parameter(s):
    None

    Returns:
    None
*/

["BSO Coop - Caching", "Enable Dynamic Caching", {
    private _dialogResult =
    [
        "Enable Dynamic Caching",
        [
            [
                "Mode", ["Activate", "Deactivate"], (parseNumber dynamicSimulationSystemEnabled)
            ]
        ]
    ] call Ares_fnc_ShowChooseDialog;
    if (_dialogResult isEqualTo []) exitWith {};
    _dialogResult params ["_value"];
    ["enableDynamicSimulationSystem", _value isEqualTo 0] call CFUNC(globalEvent);
    [QGVAR(toggleLoop), _value isEqualTo 0] call CFUNC(serverEvent);
}] call Ares_fnc_RegisterCustomModule;

["BSO Coop - Caching", "Set Dynamic Caching Distances", {
    private _dialogResult =
    [
        "Set Dynamic Caching Distances",
        [
            [
                "Group", "", (GVAR(defaultValues) select 0)
            ],
            [
                "Vehicle", "", (GVAR(defaultValues) select 1)
            ],
            [
                "EmptyVehicle", "", (GVAR(defaultValues) select 2)
            ],
            [
                "Prop", "", (GVAR(defaultValues) select 3)
            ],
            [
                "isMoving Coef", "", (GVAR(defaultValues) select 4)
            ]
        ]
    ] call Ares_fnc_ShowChooseDialog;
    if (_dialogResult isEqualTo []) exitWith {};
    _dialogResult = _dialogResult apply {parseNumber _x};
    _dialogResult params ["_group", "_vehicle", "_eVehicle", "_prop", "_mov"];
    ["setDynamicSimulationDistance", ["Group", _group]] call CFUNC(globalEvent);
    ["setDynamicSimulationDistance", ["Vehicle", _vehicle]] call CFUNC(globalEvent);
    ["setDynamicSimulationDistance", ["EmptyVehicle", _eVehicle]] call CFUNC(globalEvent);
    ["setDynamicSimulationDistance", ["Prop", _prop]] call CFUNC(globalEvent);
    ["setDynamicSimulationDistanceCoef", ["isMoving", _mov]] call CFUNC(globalEvent);
}] call Ares_fnc_RegisterCustomModule;

["BSO Coop - Caching", "Change Caching Behavoir", {
    params ["_module_position", "_selected_object"];

    private _selected_objects = if (isNull _selected_object) then {
        ["objects"] call Achilles_fnc_SelectUnits;
    } else {
        [_selected_object];
    };
    if (isNil "_selected_objects") exitWith {};
    if (_selected_objects isEqualTo []) exitWith {};

    private _dialogResult =
    [
        "Change Caching Behavoir",
        [
            [
                "Allow Caching", ["Enable", "Disable"]
            ]
        ]
    ] call Ares_fnc_ShowChooseDialog;

    if (_dialogResult isEqualTo []) exitWith {};
    private _enable = ((_dialogResult select 0) == 0);

    {
        (group _x) setVariable [QGVAR(noCache), _enable, true];
        {
            _x setVariable [QGVAR(noCache), _enable, true];
        } count (units _x);
        nil
    } count _selected_objects;
}] call Ares_fnc_RegisterCustomModule;

["BSO Coop - Caching Advanced", "Add/Remove Items to Dynamic Caching", {
    params ["_center_pos", "_selected_object"];
    private _center_pos = position _logic;

    private _dialogResult =
    [
        "Add/Remove Items to Dynamic Caching",
        [
            [
                "Mode", ["Add", "Remove"]
            ],
            [
                "Range" ,["Radius", "All Mission Objects"]
            ],
            [
                "Radius","","50"
            ],
            [
                "Type",["All", "Units", "Vehicles", "Static Objects", "Logics"]
            ],
            [
                "Mode", ["All", "Side"]
            ],
            [
                "Side", "SIDE"
            ]
        ],
        "Achilles_fnc_RscDisplayAttributes_editableObjects"
    ] call Ares_fnc_ShowChooseDialog;

    if (_dialogResult isEqualTo []) exitWith {};
    private _addObject = ((_dialogResult select 0) == 0);
    private _range_mode = _dialogResult select 1;
    private _obj_type = _dialogResult select 3;

    private _objectsToProcess = [];

    if (_range_mode == 0) then {
        private _radius = parseNumber (_dialogResult select 2);
        _objectsToProcess = switch (_obj_type) do {
            case 0: {nearestObjects [_center_pos, [],_radius, true]};
            case 1: {
                private _units = nearestObjects [_center_pos, ["Man","LandVehicle","Air","Ship"], _radius, true];
                if (_dialogResult select 4 == 1) then {
                    private _side = [(_dialogResult select 5) - 1] call BIS_fnc_sideType;
                    _units select {(side _x) isEqualTo _side and count crew _x > 0};
                } else {
                    _units select {count crew _x > 0};
                };
            };
            case 2: {nearestObjects [_center_pos, ["LandVehicle","Air","Ship"], _radius, true]};
            case 3: {nearestObjects [_center_pos, ["Static"], _radius, true]};
            case 4: {nearestObjects [_center_pos, ["Logic"], _radius, true]};
        };
    } else {
        _objectsToProcess = switch (_obj_type) do {
            case 0: {allMissionObjects ""};
            case 1: {
                private _units = (allUnits + vehicles);
                if (_dialogResult select 4 == 1) then {
                    private _side = [(_dialogResult select 5) - 1] call BIS_fnc_sideType;
                    _units select {(side _x) isEqualTo _side};
                } else {
                    _units;
                };
            };
            case 2: {vehicles};
            case 3: {allMissionObjects "Static"};
            case 4: {allMissionObjects "Logic"};
        };
    };

    // protect the main curator module from deletion
    _objectsToProcess = _objectsToProcess select {typeOf _x != "ModuleCurator_F" && !isSimpleObject _x};

    private _count = {
        ["enableDynamicSimulation", [_x, _addObject]] call CFUNC(serverEvent);
        if (_x isKindOf "CAManBase") then {
            ["enableDynamicSimulation", [group _x, _addObject]] call CFUNC(serverEvent);
        };
        true
    } count _objectsToProcess;

    private _displayText = ["%1 Objects got Added to Dynamic Caching", "%1 Objects got Removed from Dynamic Caching"] select !(_addObject);
    [objNull, format [_displayText, _count]] call BIS_fnc_showCuratorFeedbackMessage;

    nil
}] call Ares_fnc_RegisterCustomModule;

["BSO Coop - Caching Advanced", "Add/Remove Item to Dynamic Caching", {

    params ["_module_position", "_selected_object"];

    private _selected_objects = if (isNull _selected_object) then {
        ["objects"] call Achilles_fnc_SelectUnits;
    } else {
        [_selected_object];
    };
    if (isNil "_selected_objects") exitWith {};
    if (_selected_objects isEqualTo []) exitWith {
        ["No object was selected!"] call Ares_fnc_ShowZeusMessage;
        playSound "FD_Start_F";
    };

    private _dialogResult =
    [
        "Add/Remove Items to Dynamic Caching",
        [
            [
                "Mode", ["Add", "Remove"]
            ]
        ]
    ] call Ares_fnc_ShowChooseDialog;

    if (_dialogResult isEqualTo []) exitWith {};
    private _addObject = ((_dialogResult select 0) == 0);

    {
        ["enableDynamicSimulation", [_x, _addObject]] call CFUNC(serverEvent);
        nil
    } count _selected_objects;
    nil
}] call Ares_fnc_RegisterCustomModule;

["BSO Coop - Caching Advanced", "Trigger Dynamic Caching", {
    params ["", "_selected_object"];

    private _selected_objects = if (isNull _selected_object) then {
        ["objects"] call Achilles_fnc_SelectUnits;
    } else {
        [_selected_object];
    };

    if (isNil "_selected_objects") then {
        _selected_objects = allPlayers;
    };
    if (_selected_objects isEqualTo []) then {
        _selected_objects = allPlayers;
    };

    private _dialogResult =
    [
        "Trigger Dynamic Caching",
        [
            [
                "Mode", ["Enable", "Disable"]
            ]
        ]
    ] call Ares_fnc_ShowChooseDialog;

    if (_dialogResult isEqualTo []) exitWith {};
    private _enable = ((_dialogResult select 0) == 0);

    {
        ["triggerDynamicSimulation", [_x, _enable]] call CFUNC(globalEvent);
        nil
    } count _selected_objects;
}] call Ares_fnc_RegisterCustomModule;

["BSO Coop - Caching Advanced", "Trigger Dynamic Caching all Players", {
    private _dialogResult =
    [
        "Trigger Dynamic Caching",
        [
            [
                "Mode", ["Enable", "Disable"]
            ]
        ]
    ] call Ares_fnc_ShowChooseDialog;

    if (_dialogResult isEqualTo []) exitWith {};
    private _enable = ((_dialogResult select 0) == 0);

    {
        ["triggerDynamicSimulation", [_x, _enable]] call CFUNC(globalEvent);
        nil
    } count allPlayers;
}] call Ares_fnc_RegisterCustomModule;

["BSO Coop - Caching Advanced", "Update Dynamic Caching Positions", {
    {
        if (dynamicSimulationEnabled _x) then {
            ["enableDynamicSimulation", [_x, false]] call CFUNC(serverEvent);
            [{
                ["enableDynamicSimulation", [_this, true]] call CFUNC(serverEvent);
            }, 1, _x] call CFUNC(wait);
        };
    } count (entities [[], ["Logic", "LaserTarget"], true, true]);
}] call Ares_fnc_RegisterCustomModule;
