#include "macros.hpp"
/*
    BSO Coop

    Author: joko // Jonas

    Description:
    initilize EarPlug System

    Parameter(s):
    None

    Returns:
    None
*/
GVAR(soundVolumeReduced) = 2;
DFUNC(ToggleEarPlugs) = {

    GVAR(soundVolumeReduced) = GVAR(soundVolumeReduced) + 1;
    GVAR(soundVolumeReduced) = GVAR(soundVolumeReduced) mod 3;

    switch (GVAR(soundVolumeReduced)) do {
        case (0): {
            0.5 fadeSound (1 * 0.5);
            hintSilent "Earplugs 50%";
        };
        case (1): {
            0.5 fadeSound (1 * 0.15);
            hintSilent "Earplugs 75%";
        };
        case (2): {
            0.5 fadeSound 1;
            hintSilent "Earplugs Out";
        };
        default {
            hintSilent "You Did something Wrong!";
        };
    };
    playSound "ClickSoft";
    [{
        hintSilent "";
    }, 10] call CFUNC(wait);
};

[ 
    QGVAR(showAction),
    "CHECKBOX",
    ["Show Earplugs Action", "Earplugs can still be changed with keybind."],
    "BSO Coop",
    true,
    2
] call CBA_Settings_fnc_init;

[
    "Change Earplugs",
    CLib_Player,
    0,
    {GVAR(showAction)},
    {
        call FUNC(ToggleEarPlugs);
    }, ["showWindow", false, "ignoredCanInteractConditions", ["isNotInVehicle"]]
] call CFUNC(addAction);

["playerChanged", {
    GVAR(soundVolumeReduced) = -1;
    0.5 fadeSound 1;
}] call CFUNC(addEventhandler);

// Keybindings

#include "\a3\editor_f\Data\Scripts\dikCodes.h"
[
    "BSO Coop",
    "Toggle_EarPlugs",
    "Toggle EarPlugs", {
        call FUNC(ToggleEarPlugs);
    }, "", [DIK_F1, [false, false, false]]
] call CBA_fnc_addKeybind;
